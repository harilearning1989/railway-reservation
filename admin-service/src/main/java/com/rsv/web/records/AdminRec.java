package com.rsv.web.records;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

public record AdminRec(
        int id,
        String adminId,
        @NotBlank(message = "firstname is not be null and blank")
        String firstName,
        @NotBlank(message = "lastname is not be null and blank")
        String lastName,
        @NotBlank(message = "username is not be null and blank")
        String username,
        @Email(message = "Invalid email")
        String email,
        //@NotBlank(message = "Invalid Phone number: Empty number")
        //@NotNull(message = "Invalid Phone number: Number is NULL")
        //@Pattern(regexp = "^\\d{10}$", message = "Invalid phone number")
        String phone,
        int age,
        String gender,//enum
        //String bloodGroup,//enum
        String stateName,
        String createdDate,
        String updatedDate
) {
}

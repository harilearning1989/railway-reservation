package com.rsv.web.records;

public record AddressRecord(
        int addrId,
        String street,
        String city,
        String state,
        int zip
) {
}

package com.rsv.web.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;

import java.util.Date;

@Entity
@Table(name = "RSV_ADMINS")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class Admin {

    @Id
    @Column(name = "id")
    //@SequenceGenerator(name = "PATIENT_DETAILS_ID_SEQ", sequenceName = "PATIENT_DETAILS_ID_SEQ", allocationSize = 1)
    //@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "PATIENT_DETAILS_ID_SEQ")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    @Column(name = "ADMIN_ID")
    private String adminId;
    @NotBlank
    @Size(max = 25)
    @Column(name = "FIRST_NAME")
    private String firstName;
    @NotBlank
    @Size(max = 25)
    @Column(name = "LAST_NAME")
    private String lastname;
    @NotBlank
    @Size(max = 25)
    @Column(name = "USERNAME")
    private String username;
    @NotBlank
    @Size(max = 100)
    @Column(name = "PASSWORD")
    private String password;
    @NaturalId
    @NotBlank
    @Size(max = 40)
    @Email
    @Column(name = "EMAIL")
    private String email;
    @Column(name = "PHONE")
    private String phone;
    @Column(name = "AGE")
    private int age;
    @Size(min = 4, max = 6)
    @Column(name = "GENDER")
    private String gender;
    @Column(name = "STATE_NAME")
    private String stateName;
    @Column(name = "CREATED_AT")
    private Date createdDate;
    @Column(name = "UPDATED_AT")
    private Date updatedDate;

}

package com.rsv.web.services;

import com.rsv.web.mapper.DataMappers;
import com.rsv.web.models.Admin;
import com.rsv.web.records.AdminRec;
import com.rsv.web.repos.AdminRepository;
import com.web.util.constants.CommonConstants;
import jakarta.servlet.http.HttpServletRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.server.ResponseStatusException;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@Service
public class AdminServiceImpl implements AdminService {

    private AdminRepository adminRepository;
    private DataMappers dataMappers;
    private HttpServletRequest httpServletRequest;

    @Autowired
    public AdminServiceImpl setHttpServletRequest(HttpServletRequest httpServletRequest) {
        this.httpServletRequest = httpServletRequest;
        return this;
    }

    @Autowired
    public AdminServiceImpl setPatientRepository(AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
        return this;
    }

    @Autowired
    public AdminServiceImpl setDataMappers(DataMappers dataMappers) {
        this.dataMappers = dataMappers;
        return this;
    }

    @Override
    public AdminRec createAdmin(AdminRec adminRec) {
        Set<String> role = new HashSet<>();
        role.add("admin");

        Admin admin = dataMappers.recordToEntity(adminRec);
        admin = adminRepository.save(admin);
        return dataMappers.entityToRecord(admin);
    }

    @Override
    public List<AdminRec> findAll() {
        List<Admin> adminList = adminRepository.findAll();
        return dataMappers.entityListToRecordList(adminList);
    }

    @Override
    public AdminRec findAdminById(String adminId) {
        Optional<Admin> adminOpt = adminRepository.findByAdminId(adminId);
        return dataMappers.entityToRecord(adminOpt.get());
    }

    @Override
    public AdminRec updateAdmin(AdminRec adminRec, String adminId) {
        Optional<Admin> adminOptional = adminRepository.findByAdminId(adminId);
        if (adminOptional.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format(CommonConstants.NOT_FOUND_WITH_ID, CommonConstants.ADMIN, adminId));
        }
        Admin admin = adminOptional.get();
        dataMappers.recordToEntity(admin, adminRec);
        admin = adminRepository.save(admin);
        return dataMappers.entityToRecord(admin);
    }

    @Override
    public String deleteAdminById(String adminId) {
        Optional<Admin> adminOpt = adminRepository.findByAdminId(adminId);
        if (adminOpt.isEmpty()) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND,
                    String.format(CommonConstants.NOT_FOUND_WITH_ID, CommonConstants.ADMIN, adminId));
        }
        adminRepository.deleteByAdminId(adminId);
        Admin admin = adminOpt.get();
        return admin.getFirstName();
    }

    @Override
    public void deleteAll() {
        adminRepository.deleteAll();
    }

}

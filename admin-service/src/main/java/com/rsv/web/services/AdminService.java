package com.rsv.web.services;

import com.rsv.web.records.AdminRec;

import java.util.List;

public interface AdminService {

    AdminRec createAdmin(AdminRec adminRec);

    List<AdminRec> findAll();

    AdminRec findAdminById(String adminId);

    AdminRec updateAdmin(AdminRec adminRec, String adminId);

    String deleteAdminById(String adminId);

    void deleteAll();
}

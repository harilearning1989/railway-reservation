package com.rsv.web;

import com.rsv.web.enums.ColorEnums;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@ComponentScan(basePackages = {
		"com.web.util.*",
		"com.rsv.web.*"
})
@PropertySource("classpath:common-service-${spring.profiles.active}.properties")
public class AdminApplication {

	public static void main(String[] args) {
		String colorToCheck = "Green";
		if (isColorPresent(colorToCheck)) {
			System.out.println(colorToCheck + " is a valid color.");
			System.out.println("Description: " + getColorDescription(colorToCheck));
		} else {
			System.out.println(colorToCheck + " is not a valid color.");
		}
		SpringApplication.run(AdminApplication.class, args);
	}

	public static boolean isColorPresent(String colorName) {
		for (ColorEnums color : ColorEnums.values()) {
			if (color.name().equalsIgnoreCase(colorName)) {
				return true;
			}
		}
		return false;
	}

	public static String getColorDescription(String colorName) {
		for (ColorEnums color : ColorEnums.values()) {
			if (color.name().equalsIgnoreCase(colorName)) {
				return color.getDescription();
			}
		}
		return "Color not found";
	}
}

package com.rsv.web.controls;

import com.rsv.web.records.AdminRec;
import com.rsv.web.services.AdminService;
import com.web.util.response.RsvApiResponse;
import com.web.util.utils.ResponseUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "Admin Details", description = "Admin management APIs")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("admin")
public class AdminRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AdminRestController.class);
    private AdminService adminService;

    @Autowired
    public AdminRestController setAdminService(AdminService adminService) {
        this.adminService = adminService;
        return this;
    }

    @Operation(summary = "Create a new Admin Record", description = "Create Admin Record")
    @ApiResponses({
            @ApiResponse(responseCode = "201", content = {
                    @Content(schema = @Schema(implementation = AdminRec.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @PostMapping("/create")
    public ResponseEntity<RsvApiResponse<AdminRec>> createAdmin(@RequestBody AdminRec adminRec) {
        try {
            AdminRec admin = adminService.createAdmin(adminRec);
            return ResponseUtil.createNewTrainResponse(admin);
        } catch (Exception e) {
            return ResponseUtil.createServerErrorResponse(e.getMessage());
        }
    }

    @Operation(summary = "Retrieve all admins")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {
                    @Content(schema = @Schema(implementation = AdminRec.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "204", description = "There are no Admin", content = {
                    @Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/allAdmins")
    public ResponseEntity<RsvApiResponse<List<AdminRec>>> getAllAdmins() {
        try {
            List<AdminRec> admins = adminService.findAll();
            if (admins == null || admins.isEmpty()) {
                return ResponseUtil.createNoDataResponse("No Data Found");
            }
            return ResponseUtil.createSuccessResponse(admins);
        } catch (Exception e) {
            return ResponseUtil.createServerErrorResponse(e.getMessage());
        }
    }

    @Operation(
            summary = "Retrieve a AdminRec by Id",
            description = "Get a AdminRec object by specifying its id. The response is AdminRec object with id, " +
                    "title, description and published status.")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = AdminRec.class),
                    mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/findByAdmin/{adminId}")
    public ResponseEntity<RsvApiResponse<AdminRec>> findByAdminById(
            @PathVariable("adminId") String adminId) {
        AdminRec AdminRec = adminService.findAdminById(adminId);
        if (AdminRec != null) {
            return ResponseUtil.createSuccessResponse(AdminRec);
        } else {
            return ResponseUtil.createNoDataResponse("No Data Found");
        }
    }

    @Operation(summary = "Update a AdminRec by Id")
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {
                    @Content(schema = @Schema(implementation = AdminRec.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())})})
    @PutMapping("/update/{adminId}")
    public ResponseEntity<AdminRec> updateAdmin(@PathVariable("adminId") String adminId,
                                                @RequestBody AdminRec AdminRec) {
        AdminRec admin = adminService.updateAdmin(AdminRec, adminId);
        if (admin != null) {
            return new ResponseEntity<>(admin, HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Delete a AdminRec by Id")
    @ApiResponses({@ApiResponse(responseCode = "204", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @DeleteMapping("/deleteById/{adminId}")
    public ResponseEntity<String> deleteAdminById(@PathVariable("adminId") String adminId) {
        try {
            String adminName = adminService.deleteAdminById(adminId);
            return new ResponseEntity<>(adminName,HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Operation(summary = "Delete all Admins")
    @ApiResponses({@ApiResponse(responseCode = "204", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @DeleteMapping("/deleteAll")
    public ResponseEntity<HttpStatus> deleteAllAdmins() {
        try {
            adminService.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}

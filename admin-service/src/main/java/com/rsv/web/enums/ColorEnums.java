package com.rsv.web.enums;

public enum ColorEnums {
    RED("Selected Color Red"),
    GREEN("Selected Color Green"),
    BLUE("Selected Color Blue"),
    YELLOW("Selected Color Yellow");

    private String description;

    ColorEnums(String description) {
        this.description = description;
    }

    public String getDescription() {
        return description;
    }
}

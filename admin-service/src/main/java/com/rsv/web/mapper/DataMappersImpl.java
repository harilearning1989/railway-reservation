package com.rsv.web.mapper;

import com.rsv.web.models.Admin;
import com.rsv.web.records.AdminRec;
import org.springframework.stereotype.Component;

import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Component
public class DataMappersImpl implements DataMappers {

    @Override
    public Admin recordToEntity(AdminRec record) {
        Admin.AdminBuilder adminBuilder = Admin.builder();
        adminBuilder.adminId(record.adminId());

        return adminBuilder.build();
    }

    @Override
    public Admin recordToEntity(Admin admin, AdminRec record) {
        return null;
    }

    @Override
    public AdminRec entityToRecord(Admin admin) {
        //Wed Aug 16 12:29:39 IST 2023
        //String createdDateTmp = HospitalUtils.convertDateToString(admin.getCreatedDate());
        //String updatedDateTmp = HospitalUtils.convertDateToString(admin.getUpdatedDate());

        AdminRec record = new AdminRec(
                admin.getId(),
                admin.getAdminId(),
                admin.getFirstName(),
                admin.getLastname(),
                admin.getUsername(),
                admin.getEmail(),
                String.valueOf(admin.getPhone()),
                admin.getAge(),
                admin.getGender(),
                admin.getStateName(),
                admin.getCreatedDate().toString(),
                admin.getUpdatedDate().toString());
        return record;
    }

    @Override
    public List<AdminRec> entityListToRecordList(List<Admin> adminList) {
        return Optional.ofNullable(adminList)
                .orElseGet(Collections::emptyList)
                .stream()
                .filter(Objects::nonNull)
                .map(m -> entityToRecord(m))
                .toList();
    }

}

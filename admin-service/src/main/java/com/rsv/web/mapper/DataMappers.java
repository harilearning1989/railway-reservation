package com.rsv.web.mapper;

import com.rsv.web.models.Admin;
import com.rsv.web.records.AdminRec;

import java.util.List;

public interface DataMappers {
    Admin recordToEntity(AdminRec record);

    Admin recordToEntity(Admin admin, AdminRec record);

    AdminRec entityToRecord(Admin admin);

    List<AdminRec> entityListToRecordList(List<Admin> adminList);
}

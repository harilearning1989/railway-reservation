import {Component, OnInit} from '@angular/core';
import {CommonService} from "./services/common.service";
import {SharedService} from "./services/shared.service";

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    private commonService: CommonService,
    private sharedService: SharedService) {
  }

  ngOnInit(): void {
    this.commonService.getStates().subscribe(data => {
      this.sharedService.states = data;
    });
  }

  title = 'railway-rsva';
  userName: string | undefined;
  user_role: string | undefined;

  logout() {
    this.userName = 'hari';
    this.user_role = 'ROLE_ADMIN';
  }

}

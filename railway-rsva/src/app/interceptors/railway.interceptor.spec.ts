import { TestBed } from '@angular/core/testing';

import { RailwayInterceptor } from './railway.interceptor';

describe('RailwayInterceptor', () => {
  beforeEach(() => TestBed.configureTestingModule({
    providers: [
      RailwayInterceptor
      ]
  }));

  it('should be created', () => {
    const interceptor: RailwayInterceptor = TestBed.inject(RailwayInterceptor);
    expect(interceptor).toBeTruthy();
  });
});

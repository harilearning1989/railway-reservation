import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

const homeModule = () => import('./modules/home/home.module').then(x => x.HomeModule);
const pageNotFoundModule = () => import('./modules/page-not-found/page-not-found.module').then(x => x.PageNotFoundModule);
const scheduledTrainsModule = () => import('./modules/scheduled-trains/scheduled-trains.module').then(x => x.ScheduledTrainsModule);
const loginModule = () => import('./modules/login/login.module').then(x => x.LoginModule);
const aboutModule = () => import('./modules/about/about.module').then(x => x.AboutModule);
const contactModule = () => import('./modules/contact/contact.module').then(x => x.ContactModule);
const adminModule = () => import('./modules/admin/admin.module').then(x => x.AdminModule);
const bookingModule = () => import('./modules/ticket-booking/ticket-booking.module').then(x => x.TicketBookingModule);
const scheduleTrainModule = () => import('./modules/schedule-train/schedule-train.module').then(x => x.ScheduleTrainModule);
const userModule = () => import('./modules/user/user.module').then(x => x.UserModule);
const trainModule = () => import('./modules/train/train.module').then(x => x.TrainModule);
const addTrainModule = () => import('./modules/add-train/add-train.module').then(x => x.AddTrainModule);
const employeeModule = () => import('./modules/employee/employee.module').then(x => x.EmployeeModule);

const routes: Routes = [
  {path: 'home', loadChildren: homeModule},
  {path: 'about', loadChildren: aboutModule},
  {path: 'contact', loadChildren: contactModule},
  {path: 'login', loadChildren: loginModule},
  {path: 'admin', loadChildren: adminModule},
  {path: 'booking', loadChildren: bookingModule},
  {path: 'scheduleTrain', loadChildren: scheduleTrainModule},
  {path: 'scheduledTrains', loadChildren: scheduledTrainsModule},
  {path: 'user', loadChildren: userModule},
  {path: 'trainDetails', loadChildren: trainModule},
  {path: 'addTrain', loadChildren: addTrainModule},
  {path: 'employee', loadChildren: employeeModule},
  {path: '', redirectTo: '/home', pathMatch: 'full'},
  {path: '**', data: {title: 'Page Not Module'}, loadChildren: pageNotFoundModule}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

import {GlobalResponse} from "../models/global-response";
import {AdminDetails} from "../models/admin-details";

export class AdminMock {

  static adminDetailFirst: AdminDetails =
    {
      id: 1,
      adminId: '12345',
      age: 36,
      gender: 'Male',
      roles: 'admin',
      stateName: 'Andhra Pradesh',
      createdDate: '19-07-2024 10:34:45',
      updatedDate: '19-07-2024 10:34:45',
      firstName: 'Hari',
      lastName: 'Duddukunta',
      phone: '949496',
      username: 'hari.duddukunta',
      email: 'hari.duddukunta@example.com'
    };
  static adminDetailSecond: AdminDetails =
    {
      id: 2,
      adminId: '12346',
      age: 31,
      gender: 'Female',
      roles: 'user',
      stateName: 'Karnataka',
      createdDate: '20-06-2024 10:34:45',
      updatedDate: '20-06-2024 10:34:45',
      firstName: 'Mounika',
      lastName: 'Sane',
      phone: '949219',
      username: 'mouni.sane',
      email: 'mouni.sane@example.com'
    };

  static mockAdminDetailsArray: AdminDetails[] = [AdminMock.adminDetailFirst, AdminMock.adminDetailSecond];

  static mockResponse: GlobalResponse<AdminDetails[]> = {
    status: 'Success',
    message: 'Fetched data',
    data: AdminMock.mockAdminDetailsArray
  };

  static mockGetByIdResponse: GlobalResponse<AdminDetails> = {
    status: 'success',
    message: 'Admin details retrieved successfully',
    data: AdminMock.adminDetailFirst
  };

  static mockUpdateResponse: GlobalResponse<AdminDetails> = {
    status: 'success',
    message: 'Admin details updated successfully',
    data: AdminMock.adminDetailSecond
  };

  static mockCreateAdminResponse: GlobalResponse<AdminDetails> = {
    status: 'success',
    message: 'Admin created successfully',
    data: {...AdminMock.adminDetailFirst, roles: 'admin'}
  };

}

import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import {Employee} from "../models/employee";

@Injectable({
  providedIn: 'root'
})
export class MockEmployeeService {
  private employees: Employee[] = [
    { id: 1, name: 'John Doe', position: 'Developer', office: 'IT' },
    { id: 2, name: 'Jane Smith', position: 'Manager', office: 'HR' }
  ];

  getEmployees(): Observable<Employee[]> {
    return of(this.employees);
  }

  getEmployeeById(id: number): Observable<Employee> {
    const employee = this.employees.find(emp => emp.id === id);
    return of(employee as Employee);
  }

  addEmployee(employee: Employee): Observable<Employee> {
    this.employees.push({ ...employee, id: this.employees.length + 1 });
    return of(employee);
  }

  updateEmployee(employee: Employee): Observable<Employee> {
    const index = this.employees.findIndex(emp => emp.id === employee.id);
    this.employees[index] = employee;
    return of(employee);
  }

  deleteEmployeeById(id: number): Observable<void> {
    this.employees = this.employees.filter(emp => emp.id !== id);
    return of();
  }
}

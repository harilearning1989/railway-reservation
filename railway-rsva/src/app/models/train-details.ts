export interface TrainDetails {
  id?: number;
  trainNumber?: string;
  trainName?: string;
  totalCoach?: number;
  eachCoach?: number;
  price?: number;
  sourceLocation?: string;
  destinationLocation?: string;
}

export interface UserDetails {
  id?: number;
  userId?: string;
  firstName?: string;
  lastname?: string;
  username?: string;
  email?: string;
  phoneNumber?: string;
  age?: number;
  gender?: string;
  createdDate?: string;
  updatedDate?: string;
  stateName?: string;
  roles?: string;
}

export interface States {
  id: number,
  stateName: string;
  shortForm: string;
}

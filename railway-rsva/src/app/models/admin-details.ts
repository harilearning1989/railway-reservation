export interface AdminDetails {
  id: number;
  adminId: string;
  firstName: string;
  lastName: string;
  username: string;
  email: string;
  phone: string;
  age: number;
  gender: string;
  createdDate: string;
  updatedDate: string;
  stateName: string;
  roles: string;
}

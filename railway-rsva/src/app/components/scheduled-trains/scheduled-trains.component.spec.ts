import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ScheduledTrainsComponent } from './scheduled-trains.component';

describe('ScheduledTrainsComponent', () => {
  let component: ScheduledTrainsComponent;
  let fixture: ComponentFixture<ScheduledTrainsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ScheduledTrainsComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(ScheduledTrainsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import {Component, OnInit} from '@angular/core';
import {ScheduledTrainService} from "../../services/scheduled-train.service";
import {TrainDetails} from "../../models/train-details";
import $ from "jquery";

@Component({
  selector: 'app-scheduled-trains',
  templateUrl: './scheduled-trains.component.html',
  styleUrls: ['./scheduled-trains.component.scss']
})
export class ScheduledTrainsComponent implements OnInit {

  trainDetails?: TrainDetails[];
  dataTable: any;

  constructor(private scheduledTrainService: ScheduledTrainService) {
  }

  ngOnInit(): void {
    this.loadData();
  }

  loadData() {
    this.scheduledTrainService.getTrainDetails().subscribe(response => {
      if (response.message === 'Success') {
        this.trainDetails = response.data;
        this.initializeDataTable();
      } else {
        console.error('Error fetching users:', response.message);
      }
    });
  }
  initializeDataTable() {
    const table: any = $('#dataTable');
    this.dataTable = table.DataTable({
      data: this.trainDetails,
      columns: [
        {title: 'ID', data: 'id'},
        {title: 'Train Number', data: 'trainNumber'},
        {title: 'Train Name', data: 'trainName'},
        {title: 'Total Coach', data: 'totalCoach'},
        {title: 'Each Coach', data: 'eachCoach'},
        {title: 'Price', data: 'price'},
        {title: 'Source', data: 'sourceLocation'},
        {title: 'Destination', data: 'destinationLocation'},
      ],
      pageLength: 15,
      scrollY: 300,
      responsive: true,
    });

    $('#patientDataTable').DataTable({
      pagingType: 'full_numbers',
      pageLength: 20,
      processing: true,
      scrollCollapse: true,
      scrollY: '550px',
      lengthMenu: [5, 10, 25]
    });
  }

  ngOnDestroy() {
    if (this.dataTable) {
      this.dataTable.destroy();
    }
  }

}

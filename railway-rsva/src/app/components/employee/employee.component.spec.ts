import {ComponentFixture, TestBed} from '@angular/core/testing';

import {EmployeeComponent} from './employee.component';
import {HttpClientTestingModule} from "@angular/common/http/testing";
import {EmployeeService} from "../../services/employee.service";
import {Employee} from "../../models/employee";
import {of} from "rxjs";
import {ReactiveFormsModule} from "@angular/forms";

const dummyEmployees: Employee[] = [
  {id: 1, name: 'John', position: 'Developer', salary: 20000},
  {id: 2, name: 'Doe', position: 'Designer', salary: 10000}
];
const newEmployee: Employee = {id: 3, name: 'Michael Johnson', position: 'Tester'};

describe('EmployeeComponent', () => {
  let component: EmployeeComponent;
  let fixture: ComponentFixture<EmployeeComponent>;
  let employeeService: EmployeeService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [EmployeeComponent],
      imports: [HttpClientTestingModule, ReactiveFormsModule],
      providers: [EmployeeService]
    })
      .compileComponents();

    fixture = TestBed.createComponent(EmployeeComponent);
    component = fixture.componentInstance;
    employeeService = TestBed.inject(EmployeeService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should load employees on init', () => {
    spyOn(employeeService, 'getEmployees').and.returnValue(of(dummyEmployees));
    component.ngOnInit();
    expect(employeeService.getEmployees).toHaveBeenCalled();
    expect(component.employees).toEqual(dummyEmployees);
  });

  it('should create an employee', () => {
    spyOn(employeeService, 'createEmployee').and.returnValue(of(newEmployee));
    component.createEmployee(newEmployee);
    expect(employeeService.createEmployee).toHaveBeenCalledWith(newEmployee);
    expect(component.employees).toContain(newEmployee);
  });

  it('should get a single employee', () => {
    spyOn(employeeService, 'getEmployeeById').and.returnValue(of(newEmployee));
    component.getEmployee(3);
    expect(employeeService.getEmployeeById).toHaveBeenCalledWith(3);
    expect(component.selectedEmployee).toEqual(newEmployee);
  });

  it('should update an employee', () => {
    spyOn(employeeService, 'updateEmployee').and.returnValue(of(newEmployee));
    component.updateEmployee(newEmployee.id, newEmployee);
  });

  /*it('should update an employee', () => {
    spyOn(employeeService, 'updateEmployee').and.returnValue(of(newEmployee));
    component.updateEmployee(newEmployee.id, newEmployee);

    expect(employeeService.updateEmployee).toHaveBeenCalledWith(newEmployee.id, newEmployee);
    const index = component.employees.findIndex(e => e.id === newEmployee.id);
    expect(component.employees[index]).toEqual(newEmployee);
  });

  it('should update employee successfully', () => {
    const updatedEmployeeId = 3;
    spyOn(employeeService, 'updateEmployee').and.returnValue(of(newEmployee));
    component.updateEmployee(updatedEmployeeId, newEmployee);
    expect(component.employees.find(e => e.id === updatedEmployeeId)?.name).toBe('Updated Name');
  });*/

  it('should delete an employee', () => {
    const employeeId = 1;
    spyOn(employeeService, 'deleteEmployeeById').and.returnValue(of(0));
    component.deleteEmployeeById(employeeId);
    expect(employeeService.deleteEmployeeById).toHaveBeenCalledWith(employeeId);
    expect(component.employees.find(e => e.id === employeeId)).toBeUndefined();
  });

});

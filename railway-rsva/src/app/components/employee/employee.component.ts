import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {States} from "../../models/states";
import {SharedService} from "../../services/shared.service";
import {EmployeeService} from "../../services/employee.service";
import {Employee} from "../../models/employee";

@Component({
  selector: 'app-employee',
  templateUrl: './employee.component.html',
  styleUrls: ['./employee.component.scss']
})
export class EmployeeComponent implements OnInit {
  registrationForm: FormGroup;
  loading = false;
  states: States[] = [];
  errorMessage?: string;
  employees: Employee[] = [];
  isEditMode = false;
  currentEmployeeId?: number;
  selectedEmployee: Employee | null = null;

  constructor(private fb: FormBuilder,
              private employeeService: EmployeeService,
              private sharedService: SharedService) {
    this.registrationForm = this.fb.group({
      adminId: ['', [Validators.required, Validators.minLength(5)]],
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      username: ['', [Validators.required, Validators.minLength(5)]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required,
        Validators.pattern('^[0-9]*$'), Validators.minLength(10), Validators.maxLength(10)]],
      gender: ['', Validators.required],
      stateName: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.states = this.sharedService.states;
    this.loadEmployees();
  }

  onSubmit() {
    this.loading = true;
    if (this.registrationForm.valid) {
      console.log(this.registrationForm.value);
      this.employeeService.createEmployee(this.registrationForm.value).subscribe((data: any) => {
        if (data.status == 400) {
        } else {
          console.log("Submitted Successfully");
          this.clearForm();
        }
        this.errorMessage = data.status + ' ' + data.message;
        this.loading = false;
      });
    } else {
      console.log('Form is invalid');
      this.loading = false;
      //this.clearForm();
    }
  }

  loadEmployees(): void {
    this.employeeService.getEmployees().subscribe(data => {
      this.employees = data;
    });
  }

  getEmployee(id: number): void {
    this.employeeService.getEmployeeById(id).subscribe(employee => {
      this.selectedEmployee = employee;
    });
  }

  createEmployee(employee: Employee): void {
    this.employeeService.createEmployee(employee).subscribe(newEmployee => {
      this.employees.push(newEmployee);
    });
  }

  updateEmployee(id: number | undefined, employee: Employee): void {
    if (id !== undefined) {
      this.employeeService.updateEmployee(id, employee).subscribe(updatedEmployee => {
        const index = this.employees.findIndex(e => e.id === id);
        if (index !== -1) {
          this.employees[index] = updatedEmployee;
        }
      });
    }
  }

  deleteEmployeeById(id: number): void {
    this.employeeService.deleteEmployeeById(id).subscribe(() => {
      this.employees = this.employees.filter(e => e.id !== id);
    });
  }

  resetForm(): void {
    this.isEditMode = false;
    this.currentEmployeeId = 0;
    this.registrationForm.reset();
  }

  clearForm() {
    this.registrationForm.reset();
  }
}

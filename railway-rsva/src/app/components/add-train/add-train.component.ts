import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {States} from "../../models/states";
import {SharedService} from "../../services/shared.service";

@Component({
  selector: 'app-add-train',
  templateUrl: './add-train.component.html',
  styleUrls: ['./add-train.component.scss']
})
export class AddTrainComponent implements OnInit {

  addTrainForm: FormGroup;
  loading = false;
  states: States[] = [];
  errorMessage?: string;

  constructor(private fb: FormBuilder,
              private sharedService: SharedService) {
    this.addTrainForm = this.fb.group({
      trainNumber: ['', [Validators.required, Validators.minLength(5)]],
      trainName: ['', [Validators.required, Validators.minLength(3)]],
      totalCoach: ['', [Validators.required]],
      eachCoach: ['', [Validators.required]],
      price: ['', [Validators.required]],
      sourceLocation: ['', [Validators.required, Validators.minLength(3)]],
      destinationLocation: ['', [Validators.required, Validators.minLength(3)]]
    });
  }

  ngOnInit(): void {
    this.states = this.sharedService.states;
  }

  onSubmit() {
    this.loading = true;
    if (this.addTrainForm.valid) {
      console.log(this.addTrainForm.value);

    } else {
      console.log('Form is invalid');
      this.loading = false;
      //this.clearForm();
    }
  }

  clearForm() {
    this.addTrainForm.reset();
  }

}

import {AfterViewInit, Component, OnDestroy, OnInit} from '@angular/core';
import {CommonService} from "../../services/common.service";
import * as $ from 'jquery';
import 'datatables.net-bs5';

@Component({
  selector: 'app-data-tables-example',
  templateUrl: './data-tables-example.component.html',
  styleUrls: ['./data-tables-example.component.scss']
})
export class DataTablesExampleComponent implements OnInit, AfterViewInit, OnDestroy {

  ngOnInit(): void {
  }

  dataTable: any;
  jsonData?: any[];

  constructor(private dataService: CommonService) {
  }

  ngAfterViewInit() {
    this.dataService.fetchData().subscribe(data => {
      this.jsonData = data;
      this.initializeDataTable();
    });
  }

  initializeDataTable() {
    /*const table: any = $('#dataTable');
    this.dataTable = table.DataTable({
      data: this.jsonData,
      columns: [
        {title: 'ID', data: 'id'},
        {title: 'Name', data: 'name'},
        {title: 'Email', data: 'email'},
      ],
      pageLength: 15,
      scrollY: 300,
      responsive: true,
    });*/
  }

  ngOnDestroy() {
    if (this.dataTable) {
      this.dataTable.destroy();
    }
  }

}

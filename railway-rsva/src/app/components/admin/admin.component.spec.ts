import {ComponentFixture, TestBed} from '@angular/core/testing';
import {HttpClientTestingModule} from '@angular/common/http/testing';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {of} from 'rxjs';
import {AdminComponent} from "./admin.component";
import {AdminService} from "../../services/admin.service";
import {AdminDetails} from "../../models/admin-details";
import {AdminMock} from "../../mocks/admin-mock";
import {GlobalResponse} from "../../models/global-response";

describe('AdminComponent', () => {
  let component: AdminComponent;
  let fixture: ComponentFixture<AdminComponent>;
  let adminService: AdminService;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, FormsModule, ReactiveFormsModule],
      declarations: [AdminComponent],
      providers: [AdminService]
    })
      .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminComponent);
    component = fixture.componentInstance;
    adminService = TestBed.inject(AdminService);
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('should fetch and display admin details', () => {
    spyOn(adminService, 'getAllAdminDetails').and.returnValue(of(AdminMock.mockResponse));
    component.getAllAdminDetails();
    expect(adminService.getAllAdminDetails).toHaveBeenCalled();
    expect(component.adminDetails).toEqual(AdminMock.mockAdminDetailsArray);
    expect(component.adminDetails[0].id).toBe(1);
    expect(component.adminDetails[0].firstName).toBe('Hari');
    expect(component.adminDetails[1].id).toBe(2);
    expect(component.adminDetails[1].firstName).toBe('Mounika');
  });

  it('should get all adminDetails on init', () => {
    spyOn(adminService, 'getAllAdminDetails').and.returnValue(of(AdminMock.mockResponse));
    component.ngOnInit();
    expect(component.adminDetails).toEqual(AdminMock.mockAdminDetailsArray);
    expect(component.adminDetails[0].id).toBe(1);
    expect(component.adminDetails[0].firstName).toBe('Hari');
    expect(component.adminDetails[1].id).toBe(2);
    expect(component.adminDetails[1].firstName).toBe('Mounika');
  });

  it('should create a new admin and reset adminDetail on submit', () => {
    spyOn(adminService, 'createAdmin').and.returnValue(of(AdminMock.mockCreateAdminResponse));
    spyOn(component, 'getAllAdminDetails');

    component.isAddMode = true;
    component.adminDetail = AdminMock.adminDetailFirst;

    component.onSubmitAdmin();

    expect(adminService.createAdmin).toHaveBeenCalledWith(AdminMock.adminDetailFirst);
    expect(component.getAllAdminDetails).toHaveBeenCalled();
    expect(component.adminDetail).toEqual({} as AdminDetails);

    const createdAdmin = (adminService.createAdmin as jasmine.Spy).calls.mostRecent().args[0];
    expect(createdAdmin.id).toBe(1);
    expect(createdAdmin.firstName).toBe('Hari');

    const response = (adminService.createAdmin as jasmine.Spy).calls.mostRecent().returnValue;
    response.subscribe((res: GlobalResponse<AdminDetails>) => {
      expect(res.data.id).toBe(1);
      expect(res.data.firstName).toBe('Hari');
      expect(res.data.roles).toBe('admin');
    });
  });

  it('should update an admin and reset adminDetail on submit in edit mode', () => {
    spyOn(adminService, 'updateAdminDetails').and.returnValue(of(AdminMock.mockUpdateResponse));
    spyOn(component, 'getAllAdminDetails').and.callThrough();
    component.isAddMode = false;
    component.adminDetail = AdminMock.adminDetailSecond;

    component.onSubmitAdmin();

    expect(adminService.updateAdminDetails).toHaveBeenCalledWith(2, AdminMock.adminDetailSecond);
    expect(component.getAllAdminDetails).toHaveBeenCalled();
    expect(component.isAddMode).toBe(true);
    expect(component.adminDetail).toEqual({} as AdminDetails);

    const response = (adminService.updateAdminDetails as jasmine.Spy).calls.mostRecent().returnValue;
    response.subscribe((res: GlobalResponse<AdminDetails>) => {
      expect(res.data.id).toBe(2);
      expect(res.data.firstName).toBe('Mounika');
      expect(res.data.roles).toBe('user');
    });
  });

  it('should delete an admin and refresh admin details', () => {
    spyOn(adminService, 'deleteAdminDetailsById').and.returnValue(of(''));
    spyOn(component, 'getAllAdminDetails').and.callThrough();
    const adminId = 1;
    component.deleteAdmin(adminId);
    expect(adminService.deleteAdminDetailsById).toHaveBeenCalledWith(adminId);
    expect(component.getAllAdminDetails).toHaveBeenCalled();
  });

});


import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {States} from "../../models/states";
import {AdminService} from "../../services/admin.service";
import {SharedService} from "../../services/shared.service";
import {Router} from "@angular/router";
import {AdminDetails} from "../../models/admin-details";
import $ from "jquery";

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.scss']
})
export class AdminComponent implements OnInit {

  registrationForm: FormGroup;
  loading = false;
  states: States[] = [];
  errorMessage?: string;
  adminDetails!: AdminDetails[];
  dataTable: any;
  adminDetail: AdminDetails = {} as AdminDetails;
  isAddMode = true;

  constructor(private fb: FormBuilder,
              private adminService: AdminService,
              private sharedService: SharedService,
              private router: Router) {
    this.registrationForm = this.fb.group({
      adminId: ['', [Validators.required, Validators.minLength(5)]],
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      username: ['', [Validators.required, Validators.minLength(5)]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required,
        Validators.pattern('^[0-9]*$'), Validators.minLength(10), Validators.maxLength(10)]],
      gender: ['', Validators.required],
      stateName: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.states = this.sharedService.states;
    this.getAllAdminDetails();
    this.loadData();
  }

  loadData() {
    this.adminService.getAllAdminDetails().subscribe(response => {
      if (response.message === 'Success') {
        this.adminDetails = response.data;
        this.initializeDataTable();
      } else {
        console.error('Error fetching users:', response.message);
      }
    });
  }

  initializeDataTable() {
    const table: any = $('#dataTable');
    this.dataTable = table.DataTable({
      data: this.adminDetails,
      columns: [
        {title: 'Admin Id', data: 'adminId'},
        {
          title: 'Admin Name',
          data: null,
          render: (data: any, type: any, row: { firstName: string; lastName: string; }) => {
            return row.firstName + ' ' + row.lastName;
          }
        },
        {title: 'User Name', data: 'username'},
        {title: 'Email', data: 'email'},
        {title: 'Phone', data: 'phone'},
        {title: 'Gender', data: 'gender'},
        {title: 'CreatedAt', data: 'createdDate'},
        {title: 'UpdatedAt', data: 'updatedDate'}
      ],
      pageLength: 15,
      scrollY: 300,
      responsive: true,
    });
  }

  onSubmit() {
    this.loading = true;
    if (this.registrationForm.valid) {
      console.log(this.registrationForm.value);
      this.adminService.createAdmin(this.registrationForm.value).subscribe((data: any) => {
        if (data.status == 400) {
        } else {
          console.log("Submitted Successfully");
          this.clearForm();
        }
        this.errorMessage = data.status + ' ' + data.message;
        this.loading = false;
      });
    } else {
      console.log('Form is invalid');
      this.loading = false;
      //this.clearForm();
    }
  }

  clearForm() {
    this.registrationForm.reset();
  }

   getAllAdminDetails() {
    this.adminService.getAllAdminDetails().subscribe(response => {
      this.adminDetails = response.data;
    });
  }

  onSubmitAdmin() {
    if (this.isAddMode) {
      this.adminService.createAdmin(this.adminDetail).subscribe(() => {
        this.getAllAdminDetails();
        this.adminDetail = {} as AdminDetails;
      });
    } else {
      this.adminService.updateAdminDetails(this.adminDetail.id, this.adminDetail).subscribe(() => {
        this.getAllAdminDetails();
        this.isAddMode = true;
        this.adminDetail = {} as AdminDetails;
      });
    }
  }

  updateAdminDetails(adminDetails: AdminDetails) {
    this.adminDetail = { ...adminDetails };
    this.isAddMode = false;
  }

  deleteAdmin(id: number) {
    this.adminService.deleteAdminDetailsById(id).subscribe(() => {
      this.getAllAdminDetails();
    });
  }

}

import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {LoginService} from "../../services/login.service";

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  error?: {};

  loginForm: FormGroup;
  loading = false;
  errorMessage?: string;
  public isPasswordVisible: boolean = false;

  constructor(private fb: FormBuilder,
              private router: Router,
              private loginService: LoginService) {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required, Validators.minLength(5)]],
      password: ['', [Validators.required, Validators.minLength(8)]]
    });
  }

  ngOnInit(): void {
  }

  onSubmit() {
    if (this.loginForm.valid) {
      this.loading = true;
      console.log(this.loginForm.value);
      this.loginService.login(this.loginForm.value).subscribe((data: any) => {
        if (data.status == 400) {
          console.log('Response 400')
        } else {
          console.log("Submitted Successfully");
          this.clearForm();
        }
        this.loading = false;
        this.router.navigate(['/home']);
      }, (error) => {
        console.error('Error:', error); // Handle error response
        this.errorMessage = error;
      });

      this.loading = false;
    } else {
      console.log('Form is invalid');
      this.clearForm();
    }
  }

  public togglePasswordVisibility(): void {
    this.isPasswordVisible = !this.isPasswordVisible;
  }
  clearForm() {
    this.loginForm.reset();
    this.errorMessage = '';
  }
  logout() {
    // remove user from local storage and set current user to null
    localStorage.removeItem('user');
    //this.userSubject.next(null);
    this.router.navigate(['/login']);
  }

}

import {Component, OnInit} from '@angular/core';
import {AbstractControl, FormBuilder, FormGroup, ValidationErrors, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ContactService} from "../../services/contact.service";

@Component({
  selector: 'app-contact',
  templateUrl: './contact.component.html',
  styleUrls: ['./contact.component.scss']
})
export class ContactComponent implements OnInit {

  submitted = false;
  error?: {};

  registrationForm: FormGroup;
  loading = false;
  errorMessage?: string;

  constructor(private fb: FormBuilder,
              private router: Router,
              private contactService: ContactService) {
    this.registrationForm = this.fb.group({
      name: ['', [Validators.required, Validators.minLength(5), this.noWhitespaceValidator]],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required,
        Validators.pattern('^[0-9]*$'), Validators.minLength(10), Validators.maxLength(10)]],
      message: ['', Validators.required]
    });
  }

  ngOnInit(): void {
  }

  noWhitespaceValidator(control: AbstractControl): ValidationErrors | null {
    const isWhitespace = (control.value || '').indexOf(' ') >= 0;
    const isValid = !isWhitespace;
    return isValid ? null : {'whitespace': true};
  }

  onSubmit() {
    if (this.registrationForm.valid) {
      this.loading = true;
      console.log(this.registrationForm.value);

      this.contactService.contactForm(this.registrationForm.value)
        .subscribe((data: any) => {
            console.log("Submitted Successfully");
            this.errorMessage = 'Added Successful';
            this.loading = false;
            this.submitted = false;
            this.clearForm();
          },
          (error: any) => {
            console.log("Submission Failed ::" + error);
            this.errorMessage = error.status + ' ' + error.error.message;
            this.loading = false;
          });
      this.loading = false;
    } else {
      console.log('Form is invalid');
      this.clearForm();
    }
  }

  clearForm() {
    this.registrationForm.reset();
  }

}

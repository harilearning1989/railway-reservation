import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from "@angular/forms";
import {UserService} from "../../services/user.service";
import {States} from "../../models/states";
import {SharedService} from "../../services/shared.service";

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss']
})
export class UserComponent implements OnInit {

  userForm: FormGroup;
  loading = false;
  states: States[] = [];
  errorMessage?: string;

  constructor(private fb: FormBuilder,
              private userService: UserService,
              private sharedService: SharedService) {
    this.userForm = this.fb.group({
      userId: ['', [Validators.required, Validators.minLength(5)]],
      firstName: ['', [Validators.required, Validators.minLength(3)]],
      lastName: ['', [Validators.required, Validators.minLength(3)]],
      username: ['', [Validators.required, Validators.minLength(5)]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      email: ['', [Validators.required, Validators.email]],
      phoneNumber: ['', [Validators.required,
        Validators.pattern('^[0-9]*$'), Validators.minLength(10), Validators.maxLength(10)]],
      gender: ['', Validators.required],
      stateName: ['', Validators.required]
    });
  }

  ngOnInit(): void {
    this.states = this.sharedService.states;
  }

  onSubmit() {
    this.loading = true;
    if (this.userForm.valid) {
      console.log(this.userForm.value);
      this.userService.createUser(this.userForm.value).subscribe((data: any) => {
        if (data.status == 400) {
        } else {
          console.log("Submitted Successfully");
          this.clearForm();
        }
        this.errorMessage = data.status + ' ' + data.message;
        this.loading = false;
      }, (error) => {
        console.error('Error:', error); // Handle error response
      });
    } else {
      console.log('Form is invalid');
      this.loading = false;
      //this.clearForm();
    }
  }

  clearForm() {
    this.userForm.reset();
  }

}

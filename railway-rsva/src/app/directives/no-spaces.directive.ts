import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appNoSpaces]'
})
export class NoSpacesDirective {

  constructor() { }

  @HostListener('input', ['$event']) onInputChange(event: InputEvent): void {
    const inputElement = event.target as HTMLInputElement;
    let value = inputElement.value;
    value = value.replace(/\s/g, ''); // Remove spaces
    inputElement.value = value;
    if (inputElement.value !== value) {
      event.stopPropagation();
    }
  }
}

import { Directive, HostListener } from '@angular/core';

@Directive({
  selector: '[appOnlyAlphabets]'
})
export class OnlyAlphabetsDirective {

  constructor() { }

  @HostListener('input', ['$event']) onInputChange(event: InputEvent): void {
    const inputElement = event.target as HTMLInputElement;
    let value = inputElement.value;
    value = value.replace(/[^a-zA-Z]/g, ''); // Remove non-alphabetic characters
    inputElement.value = value;
    if (inputElement.value !== value) {
      event.stopPropagation();
    }
  }
}

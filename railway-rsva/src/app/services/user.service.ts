import { Injectable } from '@angular/core';
import {AdminDetails} from "../models/admin-details";
import {Observable} from "rxjs";
import {UserDetails} from "../models/user-details";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class UserService {

  private httpLink = {
    saveUser: environment.loginServiceUrl + 'auth/user/create',
    allUsers: environment.apiUrl + 'admin/allAdmins',
    listAllUsers: environment.apiUrl + 'user/allUsers',
    userById: environment.apiUrl + 'user/byId',
  }

  constructor(private http: HttpClient) {
  }

  createUser(userDetails: UserDetails): Observable<UserDetails> {
    userDetails.roles = 'user';
    return this.http.post<AdminDetails>(this.httpLink.saveUser, userDetails);
  }

  getUserDetails(): Observable<UserDetails[]> {
    return this.http.get<UserDetails[]>(`${this.httpLink.listAllUsers}`);
  }

  getUserDetailsById(id: number): Observable<UserDetails> {
    return this.http.get<UserDetails>(`${this.httpLink.userById}/${id}`);
  }

  createUserDetails(employee: UserDetails): Observable<UserDetails> {
    return this.http.post<UserDetails>(`${this.httpLink}`, employee);
  }

  updateUserDetails(id: number, employee: UserDetails): Observable<UserDetails> {
    return this.http.put<UserDetails>(`${this.httpLink}/${id}`, employee);
  }

  deleteUserDetails(id: number): Observable<void> {
    return this.http.delete<void>(`${this.httpLink}/${id}`);
  }
}

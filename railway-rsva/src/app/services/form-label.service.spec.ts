import { TestBed } from '@angular/core/testing';

import { FormLabelService } from './form-label.service';

describe('FormLabelService', () => {
  let service: FormLabelService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(FormLabelService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import { Injectable } from '@angular/core';
import {States} from "../models/states";
import {CommonService} from "./common.service";

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  states: States[] = [];

  constructor(
    private commonService: CommonService
  ) { }
}

import { TestBed } from '@angular/core/testing';

import { ScheduledTrainService } from './scheduled-train.service';

describe('ScheduledTrainService', () => {
  let service: ScheduledTrainService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ScheduledTrainService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});

import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {BehaviorSubject, map, Observable} from "rxjs";
import {HttpClient} from "@angular/common/http";
import {SignInRequest} from "../models/sign-in-request";
import {LoginRequest} from "../models/login-request";
import {LoginResponse} from "../models/login-response";

@Injectable({
  providedIn: 'root'
})
export class LoginService {

  private httpLink = {
    loginUrl: environment.loginServiceUrl + 'auth/signIn',
    registerUrl: environment.apiUrl + 'register'
  }

  private userSubject: BehaviorSubject<SignInRequest | null>;
  public user: Observable<SignInRequest | null>;

  constructor(
    private http: HttpClient
  ) {
    this.userSubject = new BehaviorSubject(JSON.parse(localStorage.getItem('user')!));
    this.user = this.userSubject.asObservable();
  }

  login1(username: string, password: string) {
    return this.http.post<SignInRequest>(this.httpLink.loginUrl, {username, password})
      .pipe(map(user => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        console.log(user)
        user.username = username;
        localStorage.setItem('user', JSON.stringify(user));
        this.userSubject.next(user);
        return user;
      }));
  }

  login(credentials: LoginRequest) {
    return this.http.post<LoginResponse>(this.httpLink.loginUrl, credentials)
      .pipe(map(response => {
        // store user details and jwt token in local storage to keep user logged in between page refreshes
        console.log(response)
        localStorage.setItem('user', JSON.stringify(response));
        return response;
      }));
  }
}

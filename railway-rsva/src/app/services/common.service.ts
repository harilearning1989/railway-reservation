import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {States} from "../models/states";

@Injectable({
  providedIn: 'root'
})
export class CommonService {

  private jsonUrl = 'assets/json/States.json'; // Path to your JSON file
  constructor(private http: HttpClient) {
  }

  fetchData(): Observable<any[]> {
    return this.http.get<any[]>('/assets/data.json');
  }

  getStates(): Observable<States[]> { // Use the model
    return this.http.get<States[]>(this.jsonUrl);
  }
}

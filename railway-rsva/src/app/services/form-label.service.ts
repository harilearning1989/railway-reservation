import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FormLabelService {

  private apiUrl = 'http://localhost:8084/emp/form-labels';

  constructor(private http: HttpClient) { }

  getFormLabels(): Observable<any> {
    return this.http.get<any>(this.apiUrl);
  }
}


import {Injectable} from '@angular/core';

import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Router} from "@angular/router";
import {environment} from "../../environments/environment";
import {LoginRequest} from "../models/login-request";
import {LoginResponse} from "../models/login-response";

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  //private jwtHelper = new JwtHelperService();
  private baseUrl = environment.apiUrl + '/authenticate';

  constructor(private http: HttpClient, private router: Router) {
  }

  register(user: any): Observable<any> {
    return this.http.post(`${this.baseUrl}/register`, user);
  }

  logout() {
    localStorage.removeItem('token');
    this.router.navigate(['/login']);
  }

  isLoggedIn() {
    /*const token = localStorage.getItem('token');
    return !this.jwtHelper.isTokenExpired(token);*/
    return !!localStorage.getItem('token');
  }

  getToken() {
    return localStorage.getItem('token');
  }

}


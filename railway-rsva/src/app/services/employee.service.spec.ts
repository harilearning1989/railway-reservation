import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {EmployeeService} from './employee.service';
import {Employee} from "../models/employee";

const dummyEmployees: Employee[] = [
  {id: 1, name: 'John', position: 'Developer', salary: 20000},
  {id: 2, name: 'Doe', position: 'Designer', salary: 10000}
];
const dummyEmployee: Employee = {id: 1, name: 'John', position: 'Developer'};
const newEmployee: Employee = {id: 3, name: 'Jane', position: 'Tester'};
const updatedEmployee: Employee = {id: 1, name: 'John', position: 'Senior Developer'};

describe('EmployeeService', () => {
  let service: EmployeeService;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [EmployeeService]
    });
    service = TestBed.inject(EmployeeService);
    httpMock = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  afterEach(() => {
    httpMock.verify();
  });

  it('should retrieve all employees', () => {
    service.getEmployees().subscribe(employees => {
      expect(employees.length).toBe(2);
      expect(employees).toEqual(dummyEmployees);
    });

    const req = httpMock.expectOne(`${service['apiUrl']}`);
    expect(req.request.method).toBe('GET');
    req.flush(dummyEmployees);
  });

  it('should retrieve an employee by id', () => {
    service.getEmployeeById(1).subscribe(employee => {
      expect(employee).toEqual(dummyEmployee);
    });

    const req = httpMock.expectOne(`${service['apiUrl']}/1`);
    expect(req.request.method).toBe('GET');
    req.flush(dummyEmployee);
  });

  it('should create an employee', () => {
    service.createEmployee(newEmployee).subscribe(response => {
      expect(response).toEqual(newEmployee);
    });

    const req = httpMock.expectOne(`${service['apiUrl']}`);
    expect(req.request.method).toBe('POST');
    req.flush(newEmployee);
  });

  it('should update an employee', () => {
    service.updateEmployee(1, updatedEmployee).subscribe(response => {
      expect(response).toEqual(updatedEmployee);
    });

    const req = httpMock.expectOne(`${service['apiUrl']}/1`);
    expect(req.request.method).toBe('PUT');
    req.flush(updatedEmployee);
  });

  it('should delete an employee', () => {
    service.deleteEmployeeById(1).subscribe(response => {
      expect(response).toBe('Employee deleted successfully');
    });

    const req = httpMock.expectOne(`${service['apiUrl']}/1`);
    expect(req.request.method).toBe('DELETE');
    req.flush('Employee deleted successfully');
  });
});


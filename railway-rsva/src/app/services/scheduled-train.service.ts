import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {GlobalResponse} from "../models/global-response";
import {TrainDetails} from "../models/train-details";
import {environment} from "../../environments/environment";

@Injectable({
  providedIn: 'root'
})
export class ScheduledTrainService {

  private httpLink = {
    allTrains: environment.trainServiceUrl + 'train/allTrains',
    registerUrl: environment.trainServiceUrl + 'register',
  }
  trainDetails?: TrainDetails[];

  constructor(private http: HttpClient) {
  }

  getTrainDetails(): Observable<GlobalResponse<TrainDetails[]>> {
    return this.http.get<GlobalResponse<TrainDetails[]>>(this.httpLink.allTrains);
  }

}

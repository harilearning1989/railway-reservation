import {Injectable} from '@angular/core';
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {GlobalResponse} from "../models/global-response";
import {AdminDetails} from "../models/admin-details";

@Injectable({
  providedIn: 'root'
})
export class AdminService {

  httpLink = {
    saveAdmin: environment.loginServiceUrl + 'auth/admin/create',
    allAdmins: environment.adminServiceUrl + 'admin/allAdmins',
    adminById: environment.adminServiceUrl + 'admin/byId',
    updateAdminById: environment.adminServiceUrl + 'admin/update/byId',
    deleteById: environment.adminServiceUrl + 'admin/delete/byId'
  }

  constructor(private http: HttpClient) {
  }

  createAdmin(adminDetails: AdminDetails): Observable<GlobalResponse<AdminDetails>> {
    adminDetails.roles = 'admin';
    return this.http.post<GlobalResponse<AdminDetails>>(this.httpLink.saveAdmin, adminDetails);
  }

  getAllAdminDetails(): Observable<GlobalResponse<AdminDetails[]>> {
    return this.http.get<GlobalResponse<AdminDetails[]>>(this.httpLink.allAdmins);
  }

  getAdminDetailsById(id: number): Observable<GlobalResponse<AdminDetails>> {
    return this.http.get<GlobalResponse<AdminDetails>>(`${this.httpLink.adminById}/${id}`);
  }

  updateAdminDetails(id: number, adminDetails: AdminDetails): Observable<GlobalResponse<AdminDetails>> {
    return this.http.put<GlobalResponse<AdminDetails>>(`${this.httpLink.updateAdminById}/${id}`, adminDetails);
  }

  deleteAdminDetailsById(id: number): Observable<any> {
    return this.http.delete(`${this.httpLink.deleteById}/${id}`, { responseType: 'text' });
  }

}

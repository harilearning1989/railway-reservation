import {TestBed} from '@angular/core/testing';
import {HttpClientTestingModule, HttpTestingController} from '@angular/common/http/testing';
import {AdminService} from './admin.service';
import {AdminMock} from "../mocks/admin-mock";

describe('AdminService', () => {
  let service: AdminService;
  let httpTestingController: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [AdminService]
    });

    service = TestBed.inject(AdminService);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  afterEach(() => {
    httpTestingController.verify();
  });

  it('should retrieve all admin details', () => {
    service.getAllAdminDetails().subscribe(response => {
      expect(response).toEqual(AdminMock.mockResponse);
      expect(response.data.length).toBe(2);
      expect(response.data[0].firstName).toBe('Hari');
    });

    const req = httpTestingController.expectOne(service.httpLink.allAdmins);
    expect(req.request.method).toBe('GET');
    req.flush(AdminMock.mockResponse);
  });

  it('should create admin with correct roles', () => {
    service.createAdmin(AdminMock.adminDetailFirst).subscribe((res) => {
      expect(res).toEqual(AdminMock.mockCreateAdminResponse);
      expect(res.data.lastName).toBe('Duddukunta');
      expect(res.data.roles).toBe('admin');
    });

    const req = httpTestingController.expectOne(service['httpLink'].saveAdmin);
    expect(req.request.method).toBe('POST');
    expect(req.request.body.roles).toBe('admin');
    req.flush(AdminMock.mockCreateAdminResponse);
  });

  it('should fetch admin details by id and validate fields', () => {
    const mockId = 1;

    service.getAdminDetailsById(mockId).subscribe((response) => {
      expect(response.status).toBe('success');
      expect(response.message).toBe('Admin details retrieved successfully');
      expect(response.data.id).toBe(mockId);
      expect(response.data.firstName).toBe('Hari');
      expect(response.data.lastName).toBe('Duddukunta');
      expect(response.data.email).toBe('hari.duddukunta@example.com');
    });

    const req = httpTestingController.expectOne(`${service.httpLink.adminById}/${mockId}`);
    expect(req.request.method).toBe('GET');
    req.flush(AdminMock.mockGetByIdResponse);
  });

  it('should update admin details and validate fields', () => {
    const mockId = 2;

    service.updateAdminDetails(mockId, AdminMock.adminDetailSecond).subscribe((response) => {
      expect(response.status).toBe('success');
      expect(response.message).toBe('Admin details updated successfully');
      expect(response.data.id).toBe(mockId);
      expect(response.data.firstName).toBe('Mounika');
      expect(response.data.lastName).toBe('Sane');
      expect(response.data.email).toBe('mouni.sane@example.com');
    });

    const req = httpTestingController.expectOne(`${service.httpLink.updateAdminById}/${mockId}`);
    expect(req.request.method).toBe('PUT');
    expect(req.request.body).toEqual(AdminMock.adminDetailSecond);
    req.flush(AdminMock.mockUpdateResponse);
  });

  it('should delete admin details by id and return success message', () => {
    const mockId = 1;
    const mockResponse = 'Admin details deleted successfully';

    service.deleteAdminDetailsById(mockId).subscribe((response) => {
      expect(response).toBe(mockResponse);
    });

    const req = httpTestingController.expectOne(`${service.httpLink.deleteById}/${mockId}`);
    expect(req.request.method).toBe('DELETE');
    req.flush(mockResponse);
  });

});


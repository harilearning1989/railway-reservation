import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScheduleTrainRoutingModule } from './schedule-train-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ScheduleTrainRoutingModule
  ]
})
export class ScheduleTrainModule { }

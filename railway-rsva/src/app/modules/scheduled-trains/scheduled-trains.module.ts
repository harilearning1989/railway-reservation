import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ScheduledTrainsRoutingModule } from './scheduled-trains-routing.module';
import { ScheduledTrainsComponent } from '../../components/scheduled-trains/scheduled-trains.component';


@NgModule({
  declarations: [
    ScheduledTrainsComponent
  ],
  imports: [
    CommonModule,
    ScheduledTrainsRoutingModule
  ]
})
export class ScheduledTrainsModule { }

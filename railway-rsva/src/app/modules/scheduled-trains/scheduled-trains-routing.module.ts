import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {ScheduledTrainsComponent} from "../../components/scheduled-trains/scheduled-trains.component";

const routes: Routes = [
  {path: '', component: ScheduledTrainsComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ScheduledTrainsRoutingModule { }

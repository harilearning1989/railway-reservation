import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {OnlyNumbersDirective} from "../../directives/only-numbers.directive";
import {OnlyAlphabetsDirective} from "../../directives/only-alphabets.directive";
import {NoSpacesDirective} from "../../directives/no-spaces.directive";


@NgModule({
  declarations: [
    OnlyNumbersDirective,
    OnlyAlphabetsDirective,
    NoSpacesDirective
  ],
  imports: [
    CommonModule
  ],
  exports: [
    OnlyNumbersDirective,
    OnlyAlphabetsDirective,
    NoSpacesDirective
  ]
})
export class SharedModule {
}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AddTrainRoutingModule } from './add-train-routing.module';
import { AddTrainComponent } from '../../components/add-train/add-train.component';
import {ReactiveFormsModule} from "@angular/forms";
import {SharedModule} from "../shared/shared.module";


@NgModule({
  declarations: [
    AddTrainComponent
  ],
  imports: [
    CommonModule,
    AddTrainRoutingModule,
    ReactiveFormsModule,
    SharedModule
  ]
})
export class AddTrainModule { }

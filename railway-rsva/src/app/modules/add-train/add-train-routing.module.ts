import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {AddTrainComponent} from "../../components/add-train/add-train.component";

const routes: Routes = [
  {path: '', component: AddTrainComponent}
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AddTrainRoutingModule {
}

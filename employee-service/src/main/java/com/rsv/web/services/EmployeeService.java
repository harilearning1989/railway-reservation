package com.rsv.web.services;

import com.rsv.web.dtos.EmployeeRecordDTO;

import java.util.List;

public interface EmployeeService {
    List<EmployeeRecordDTO> getAllEmployees();

    List<EmployeeRecordDTO> findTopNBySalary(int n);

    EmployeeRecordDTO saveEmployee(EmployeeRecordDTO employeeRecordDTO);

    EmployeeRecordDTO updateEmployee(Integer id, EmployeeRecordDTO employeeRecordDTO);

    void deleteEmployee(Integer id);

    EmployeeRecordDTO getEmployeeById(Integer id);
}

package com.rsv.web.services;

import com.rsv.web.dtos.EmployeeRecordDTO;
import com.rsv.web.models.Employee;
import com.rsv.web.repos.EmployeeRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class EmployeeServiceImpl implements EmployeeService {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmployeeServiceImpl.class);

    @Autowired
    private EmployeeRepository employeeRepository;

    @Override
    public List<EmployeeRecordDTO> getAllEmployees() {
        LOGGER.info("Getting all employees");
        return employeeRepository.findAll().stream()
                .map(this::convertToEmployeeRecordDTO)
                .collect(Collectors.toList());
    }

    @Override
    public List<EmployeeRecordDTO> findTopNBySalary(int n) {
        LOGGER.info("Getting top N employees");
        PageRequest pageRequest = PageRequest.of(0, n);
        return employeeRepository.findTopNBySalary(pageRequest).stream()
                .map(this::convertToEmployeeRecordDTO)
                .collect(Collectors.toList());
    }

    @Override
    public EmployeeRecordDTO saveEmployee(EmployeeRecordDTO employeeRequestDTO) {
        LOGGER.info("Saving employee");
        Optional<Employee> existingEmployee = employeeRepository.findByEmail(employeeRequestDTO.email());
        if (existingEmployee.isPresent()) {
            throw new IllegalArgumentException("Employee with the same email already exists.");
        }
        Employee employee = convertToEmployee(employeeRequestDTO);
        Employee savedEmployee = employeeRepository.save(employee);
        LOGGER.info("Employee saved successfully");
        return convertToEmployeeRecordDTO(savedEmployee);
    }

    @Override
    public EmployeeRecordDTO updateEmployee(Integer empId, EmployeeRecordDTO empReqRecDTO) {
        LOGGER.info("Updating employee");
        Employee existingEmployee = employeeRepository.findById(empId)
                .orElseThrow(() -> new IllegalArgumentException("Employee not found"));

        if (!existingEmployee.getEmail().equals(empReqRecDTO.email())) {
            LOGGER.error("Employee with the same email already exists.");
            Optional<Employee> duplicateEmployee = employeeRepository.findByEmail(empReqRecDTO.email());
            if (duplicateEmployee.isPresent()) {
                LOGGER.info("Employee with the same email already exists");
                throw new IllegalArgumentException("Employee with the same email already exists.");
            }
        }
        existingEmployee = convertToEmployee(existingEmployee, empReqRecDTO);
        Employee updatedEmployee = employeeRepository.save(existingEmployee);
        LOGGER.info("Employee updated successfully");

        return convertToEmployeeRecordDTO(updatedEmployee);
    }

    @Override
    public void deleteEmployee(Integer id) {
        LOGGER.info("Deleting employee");
        employeeRepository.deleteById(id);
    }

    @Override
    public EmployeeRecordDTO getEmployeeById(Integer id) {
        LOGGER.info("Getting employee by id");
        Employee employeeRecord = employeeRepository.findById(id)
                .orElseThrow(() -> new IllegalArgumentException("Employee not found"));
        return convertToEmployeeRecordDTO(employeeRecord);
    }

    private EmployeeRecordDTO convertToEmployeeRecordDTO(Employee employeeRecord) {
        return new EmployeeRecordDTO(
                1,
                employeeRecord.getEmpId(),
                employeeRecord.getEmpName(),
                employeeRecord.getUsername(),
                employeeRecord.getEmail(),
                employeeRecord.getAge(),
                employeeRecord.getPhone(),
                employeeRecord.getGender(),
                employeeRecord.getSalary(),
                employeeRecord.getCreatedDate(),
                employeeRecord.getUpdatedDate()
        );
    }

    private Employee convertToEmployee(EmployeeRecordDTO employeeRequestDTO) {
        Employee.EmployeeBuilder employee = Employee.builder();
        employee.empId(employeeRequestDTO.empId());
        employee.empName(employeeRequestDTO.empName());
        employee.username(employeeRequestDTO.username());
        employee.email(employeeRequestDTO.email());
        employee.age(employeeRequestDTO.age());
        employee.phone(employeeRequestDTO.phone());
        employee.gender(employeeRequestDTO.gender());
        employee.salary(employeeRequestDTO.salary());
        employee.createdDate(new Date());
        employee.updatedDate(new Date());
        return employee.build();
    }

    private Employee convertToEmployee(Employee existingEmployee, EmployeeRecordDTO empReqRecDTO) {
        if (empReqRecDTO.empName() != null) {
            existingEmployee.setEmpName(empReqRecDTO.empName());
        }
        if (empReqRecDTO.username() != null) {
            existingEmployee.setUsername(empReqRecDTO.username());
        }
        if (empReqRecDTO.email() != null) {
            existingEmployee.setEmail(empReqRecDTO.email());
        }
        if (empReqRecDTO.age() != null) {
            existingEmployee.setAge(empReqRecDTO.age());
        }
        if (empReqRecDTO.phone() != null) {
            existingEmployee.setPhone(empReqRecDTO.phone());
        }
        if (empReqRecDTO.gender() != null) {
            existingEmployee.setGender(empReqRecDTO.gender());
        }
        if (empReqRecDTO.salary() > 1000) {
            existingEmployee.setSalary(empReqRecDTO.salary());
        }
        existingEmployee.setUpdatedDate(new Date());
        return existingEmployee;
    }
}

package com.rsv.web.repos;

import com.rsv.web.models.Employee;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface EmployeeRepository extends JpaRepository<Employee, Integer> {

    @Query("SELECT e FROM Employee e ORDER BY e.salary DESC")
    List<Employee> findTopNBySalary(Pageable pageable);

    Optional<Employee> findByEmail(String email);

}

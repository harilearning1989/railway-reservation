package com.rsv.web.dtos;

import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;

import java.util.Date;

public record EmployeeRecordDTO(
        int id,
        Integer empId,
        @NotBlank(message = "Emp name is mandatory")
        String empName,
        @NotBlank(message = "Username is mandatory")
        String username,
        @Email(message = "Email should be valid")
        @NotBlank(message = "Email is mandatory")
        String email,
        Integer age,
        String phone,
        String gender,
        @Positive(message = "Salary should be positive")
        double salary,
        Date createdDate,
        Date updatedDate
) {
}

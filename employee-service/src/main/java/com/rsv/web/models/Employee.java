package com.rsv.web.models;

import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Positive;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.Date;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name = "EMPLOYEE")
public class Employee {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "EMP_ID")
    private Integer empId;
    @NotBlank(message = "Emp Id is mandatory")
    @Column(name = "EMP_NAME")
    private String empName;
    @NotBlank(message = "Emp Name is mandatory")
    @Column(name = "USERNAME")
    private String username;
    @Email(message = "Email should be valid")
    @Column(name = "EMAIL", unique = true)
    private String email;
    @Column(name = "AGE")
    private Integer age;
    @Column(name = "PHONE")
    private String phone;
    @Column(name = "GENDER")
    private String gender;
    @Positive(message = "Salary should be positive")
    private double salary;
    @Column(name = "DATE_CREATED")
    private Date createdDate;
    @Column(name = "LAST_UPDATED")
    private Date updatedDate;

}

package com.rsv.web.controls;

import com.rsv.web.dtos.EmployeeRecordDTO;
import com.rsv.web.services.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/emp")
@CrossOrigin(origins = "*")
public class EmployeeRestController {

    @Autowired
    private EmployeeService employeeService;

    @GetMapping("/form-labels")
    public Map<String, String> getFormLabels() {
        return Map.of(
                "nameLabel", "Name",
                "emailLabel", "Email",
                "passwordLabel", "Password"
        );
    }

    @GetMapping("listEmp")
    public ResponseEntity<List<EmployeeRecordDTO>> getAllEmployees() {
        return new ResponseEntity<>(employeeService.getAllEmployees(), HttpStatus.OK);
    }

    @GetMapping("topNSalary")
    public ResponseEntity<List<EmployeeRecordDTO>> findTopNBySalary(
            @RequestParam(name = "top") int top) {
        return new ResponseEntity<>(employeeService.findTopNBySalary(top), HttpStatus.OK);
    }

    @PostMapping("/save")
    public ResponseEntity<EmployeeRecordDTO> createEmployee(
            @Validated @RequestBody EmployeeRecordDTO employeeRequestDTO) {
        return new ResponseEntity<>(employeeService.saveEmployee(employeeRequestDTO), HttpStatus.CREATED);
    }

    @PutMapping("/{id}")
    public ResponseEntity<EmployeeRecordDTO> updateEmployee(
            @PathVariable Integer id, @Validated @RequestBody EmployeeRecordDTO employeeRequestDTO) {
        return new ResponseEntity<>(employeeService.updateEmployee(id, employeeRequestDTO), HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> deleteEmployee(@PathVariable Integer id) {
        employeeService.deleteEmployee(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @GetMapping("/{id}")
    public ResponseEntity<EmployeeRecordDTO> getEmployeeById(@PathVariable Integer id) {
        return new ResponseEntity<>(employeeService.getEmployeeById(id), HttpStatus.OK);
    }
}

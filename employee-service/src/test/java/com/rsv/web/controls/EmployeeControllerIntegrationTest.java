package com.rsv.web.controls;

import com.rsv.web.models.Employee;
import com.rsv.web.repos.EmployeeRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;

import java.time.Duration;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicInteger;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EmployeeRepository employeeRepository;

    private final AtomicInteger successfulCreations = new AtomicInteger();

    private final List<String> report = new ArrayList<>();

    @BeforeEach
    public void setup() {
        employeeRepository.deleteAll();
        Employee employee = new Employee();
        employee.setEmpName("Hari");
        employee.setEmail("hari@gmail.com");
        employee.setSalary(10000);
        employee.setUsername("harireddy");
        employee.setAge(200);
        employee.setGender("Male");
        employee.setPhone("949496");
        employee.setCreatedDate(new Date());
        employee.setUpdatedDate(new Date());
        employeeRepository.save(employee);

        employee = new Employee();
        employee.setEmpName("Mouni");
        employee.setEmail("mouni@gmail.com");
        employee.setSalary(10000);
        employee.setUsername("harireddy");
        employee.setAge(200);
        employee.setGender("Male");
        employee.setPhone("949496");
        employee.setCreatedDate(new Date());
        employee.setUpdatedDate(new Date());
        employeeRepository.save(employee);
        employeeRepository.save(employee);

        successfulCreations.set(0);
        report.clear();
    }

    @Test
    public void testCreateEmployeesOverTime() throws Exception {
        final int durationInMinutes = 1;
        final AtomicInteger minuteCounter = new AtomicInteger();

        long startTime = System.currentTimeMillis();
        long endTime = startTime + durationInMinutes * 60 * 1000;

        for (int i = 0; i < durationInMinutes; i++) {
            long minuteStartTime = System.currentTimeMillis();
            long minuteEndTime = minuteStartTime + 60 * 1000;

            while (System.currentTimeMillis() < minuteEndTime) {
                String newEmployee = "{\"empName\":\"Hari\",\"username\":\"hari\",\"email\":\"hari1@gmail.com\",\"age\":35,\"phone\":\"9494\",\"gender\":\"male\",\"salary\":2000}";
                try {
                    MvcResult result = mockMvc.perform(post("/emp/save")
                                    .contentType(MediaType.APPLICATION_JSON)
                                    .content(newEmployee))
                            .andReturn();

                    if (result.getResponse().getStatus() == 201) {
                        minuteCounter.incrementAndGet();
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            int count = minuteCounter.getAndSet(0);
            LocalDateTime now = LocalDateTime.now();
            String time = now.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
            report.add(time + " - Employees successfully created (status 201): " + count);
            System.out.println(time + " - Employees successfully created (status 201): " + count);
        }

        // Final report after completion
        System.out.println("Final Report:");
        report.forEach(System.out::println);
    }

    @Test
    public void testCreateEmployeesOverTimeSchedule() throws Exception {
        final int durationInMinutes = 5;
        final int intervalInSeconds = 1;

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        Instant startTime = Instant.now();

        Runnable task = () -> {
            String newEmployee = "{\"empName\":\"Hari\",\"username\":\"hari\",\"email\":\"hari1@gmail.com\",\"age\":35,\"phone\":\"9494\",\"gender\":\"male\",\"salary\":2000}";
            try {
                MvcResult result = mockMvc.perform(post("/emp/save")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(newEmployee))
                        .andReturn();

                if (result.getResponse().getStatus() == 201) {
                    successfulCreations.incrementAndGet();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (Duration.between(startTime, Instant.now()).toMinutes() >= durationInMinutes) {
                scheduler.shutdown();
            }
        };

        Runnable reportTask = () -> {
            int count = successfulCreations.get();
            LocalDateTime now = LocalDateTime.now();
            String time = now.format(DateTimeFormatter.ofPattern("HH:mm:ss"));
            report.add(time + " - Employees successfully created (status 201): " + count);
            System.out.println(time + " - Employees successfully created (status 201): " + count);
        };

        scheduler.scheduleAtFixedRate(task, 0, intervalInSeconds, TimeUnit.SECONDS);
        scheduler.scheduleAtFixedRate(reportTask, 1, 1, TimeUnit.MINUTES);

        scheduler.awaitTermination(durationInMinutes + 1, TimeUnit.MINUTES);

        System.out.println("Final Report:");
        report.forEach(System.out::println);
    }

    @Test
    public void testCreateEmployeesOverTimeTmp1() throws Exception {
        final int durationInMinutes = 2;
        final int intervalInSeconds = 1;

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        Instant startTime = Instant.now();

        Runnable task = () -> {
            String newEmployee = "{\"empName\":\"Hari\",\"username\":\"hari\",\"email\":\"hari1@gmail.com\",\"age\":35,\"phone\":\"9494\",\"gender\":\"male\",\"salary\":2000}";
            try {
                MvcResult result = mockMvc.perform(post("/emp/save")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(newEmployee))
                        .andReturn();

                System.out.println("Status::"+result.getResponse().getStatus());
                if (result.getResponse().getStatus() == 201) {
                    successfulCreations.incrementAndGet();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (Duration.between(startTime, Instant.now()).toMinutes() >= durationInMinutes) {
                scheduler.shutdown();
            }
        };

        Runnable reportTask = () -> {
            int count = successfulCreations.get();
            System.out.println("Employees successfully created (status 201): " + count);
        };

        scheduler.scheduleAtFixedRate(task, 0, intervalInSeconds, TimeUnit.SECONDS);
        scheduler.scheduleAtFixedRate(reportTask, 0, 1, TimeUnit.MINUTES);

        scheduler.awaitTermination(durationInMinutes + 1, TimeUnit.MINUTES);
    }

    @Test
    public void testCreateEmployee() throws Exception {
        String newEmployee = "{\"empName\":\"Hari\",\"username\":\"hari\",\"email\":\"hari1@gmail.com\",\"age\":35,\"phone\":\"9494\",\"gender\":\"male\",\"salary\":2000}";

        mockMvc.perform(post("/emp/save")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(newEmployee))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.empName").value("Hari"))
                .andExpect(jsonPath("$.salary").value(2000));
    }

    @Test
    public void testCreateEmployeesOverTimeTmp() throws Exception {
        final int durationInMinutes = 2;
        final int intervalInSeconds = 1;

        ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
        Instant startTime = Instant.now();

        Runnable task = () -> {
            String newEmployee = "{\"empName\":\"Hari\",\"username\":\"hari\",\"email\":\"hari1@gmail.com\",\"age\":35,\"phone\":\"9494\",\"gender\":\"male\",\"salary\":2000}";
            try {
                mockMvc.perform(post("/emp/save")
                                .contentType(MediaType.APPLICATION_JSON)
                                .content(newEmployee))
                        .andExpect(status().isOk());
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (Duration.between(startTime, Instant.now()).toMinutes() >= durationInMinutes) {
                scheduler.shutdown();
            }
        };

        Runnable reportTask = () -> {
            long count = employeeRepository.count();
            System.out.println("Employees created: " + count);
        };

        scheduler.scheduleAtFixedRate(task, 0, intervalInSeconds, TimeUnit.SECONDS);
        scheduler.scheduleAtFixedRate(reportTask, 0, 1, TimeUnit.MINUTES);

        scheduler.awaitTermination(durationInMinutes + 1, TimeUnit.MINUTES);
    }

    @Test
    public void testGetAllEmployees() throws Exception {
        mockMvc.perform(get("/api/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("John Doe"))
                .andExpect(jsonPath("$[1].name").value("Jane Smith"));
    }

    @Test
    public void testGetEmployeeById() throws Exception {
        Employee employee = employeeRepository.findAll().get(0);

        mockMvc.perform(get("/api/employees/" + employee.getEmpId()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value(employee.getEmpName()))
                .andExpect(jsonPath("$.salary").value(employee.getSalary()));
    }

    @Test
    public void testUpdateEmployee() throws Exception {
        Employee employee = employeeRepository.findAll().get(0);
        String updatedEmployee = "{\"name\": \"John Doe Updated\", \"department\": \"Engineering\", \"salary\": 65000}";

        mockMvc.perform(put("/api/employees/" + employee.getEmpId())
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(updatedEmployee))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.name").value("John Doe Updated"))
                .andExpect(jsonPath("$.salary").value(65000));
    }

    @Test
    public void testDeleteEmployee() throws Exception {
        Employee employee = employeeRepository.findAll().get(0);

        mockMvc.perform(delete("/api/employees/" + employee.getEmpId()))
                .andExpect(status().isNoContent());
    }

    @Test
    public void testGetTopNEmployeesBySalary() throws Exception {
        mockMvc.perform(get("/api/employees/top-salaries?n=1"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$[0].name").value("John Doe"));
    }
}


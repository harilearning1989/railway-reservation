package com.rsv.web;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.PropertySource;

@SpringBootApplication
@ComponentScan(basePackages = {
		"com.web.util.*",
		"com.rsv.web.*"
})
@PropertySource("classpath:common-service-${spring.profiles.active}.properties")
public class TrainServiceApplication {

	//https://www.youtube.com/watch?v=GfXUljod504
	//Swagger UI: http://localhost:8081/swagger-ui/
	//OpenAPI JSON: http://localhost:8081/v3/api-docs
	//http://localhost:8081/swagger-ui/index.html
	//http://localhost:8081/v3/api-docs

	public static void main(String[] args) {
		SpringApplication.run(TrainServiceApplication.class, args);
	}


}

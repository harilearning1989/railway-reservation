package com.rsv.web.models;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "RSV_TRAIN_DETAILS",uniqueConstraints= {
        @UniqueConstraint(columnNames = {
                "TRAIN_NUMBER"
        }),
        @UniqueConstraint(columnNames = {
                "TRAIN_NAME"
        })})
@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class TrainDetails {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "ID")
    private int id;
    @Column(name = "TRAIN_NUMBER")
    private String trainNumber;
    @Column(name = "TRAIN_NAME")
    private String trainName;
    @Column(name = "TOTAL_COACH")
    private int totalCoach;
    @Column(name = "EACH_COACH")
    private int eachCoach;
    @Column(name = "PRICE")
    private int price;
    @Column(name = "SOURCE_LOCATION")
    private String sourceLocation;
    @Column(name = "DESTINATION_LOCATION")
    private String destinationLocation;

}

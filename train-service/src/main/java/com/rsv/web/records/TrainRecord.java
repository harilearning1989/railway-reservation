package com.rsv.web.records;

public record TrainRecord(
        int id,
        String trainNumber,
        String trainName,
        int totalCoach,
        int eachCoach,
        int price,
        String sourceLocation,
        String destinationLocation) {
}

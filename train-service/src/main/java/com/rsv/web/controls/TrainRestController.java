package com.rsv.web.controls;

import com.rsv.web.records.TrainRecord;
import com.rsv.web.repos.TrainRepository;
import com.rsv.web.services.TrainService;
import com.web.util.response.RsvApiResponse;
import com.web.util.utils.ResponseUtil;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Tag(name = "TrainRecord", description = "TrainRecord management APIs")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("/train")
public class TrainRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(TrainRestController.class);

    @Autowired
    private TrainService trainService;
    @Autowired
    private TrainRepository trainRepository;


    @Operation(summary = "Create a new TrainRecord", description = "Create Train Record")//tags = {"trains", "post"}
    @ApiResponses({
            @ApiResponse(responseCode = "201", content = {
                    @Content(schema = @Schema(implementation = TrainRecord.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @PostMapping("/create")
    public ResponseEntity<RsvApiResponse<TrainRecord>> createTrainRecord(@RequestBody TrainRecord trainRecord) {
        if (trainRepository.existsByTrainNumber(trainRecord.trainNumber())) {
            LOGGER.info("Train Number Already Exists with the Number::{}", trainRecord.trainNumber());
            String response = "Train Number Already Exists with the Number::" + trainRecord.trainNumber();
            return ResponseUtil.createErrorResponse(response);
        }
        if (trainRepository.existsByTrainName(trainRecord.trainName())) {
            LOGGER.info("Train Name Already Exists with the Number::{}", trainRecord.trainName());
            String response = "Train Name Already Exists with the Number::" + trainRecord.trainName();
            return ResponseUtil.createErrorResponse(response);
        }
        try {
            TrainRecord _train = trainService.createTrainRecord(trainRecord);
            return ResponseUtil.createNewTrainResponse(_train);
        } catch (Exception e) {
            return ResponseUtil.createServerErrorResponse(e.getMessage());
        }
    }

    @Operation(summary = "Retrieve all trains")//tags = {"trains", "get", "filter"}
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {
                    @Content(schema = @Schema(implementation = TrainRecord.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "204", description = "There are no Trains", content = {
                    @Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/allTrains")
    public ResponseEntity<RsvApiResponse<List<TrainRecord>>> getAllTrains() {
        try {
            List<TrainRecord> trains = trainService.findAll();
            if (trains == null || trains.isEmpty()) {
                return ResponseUtil.createNoDataResponse("No Data Found");
            }
            return ResponseUtil.createSuccessResponse(trains);
        } catch (Exception e) {
            return ResponseUtil.createServerErrorResponse(e.getMessage());
        }
    }

    @Operation(
            summary = "Retrieve a TrainRecord by Id",
            description = "Get a TrainRecord object by specifying its id. The response is TrainRecord object with id, " +
                    "title, description and published status."
    )//tags = {"trains", "get"}
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {@Content(schema = @Schema(implementation = TrainRecord.class),
                    mediaType = "application/json")}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @GetMapping("/findByTrainNumber/{trainNumber}")
    public ResponseEntity<RsvApiResponse<TrainRecord>> findByTrainNumber(
            @PathVariable("trainNumber") String trainNumber) {
        TrainRecord trainRecord = trainService.findByTrainNumber(trainNumber);
        if (trainRecord != null) {
            return ResponseUtil.createSuccessResponse(trainRecord);
        } else {
            return ResponseUtil.createNoDataResponse("No Data Found");
        }
    }

    @Operation(summary = "Update a TrainRecord by Id")//, tags = {"trains", "put"})
    @ApiResponses({
            @ApiResponse(responseCode = "200", content = {
                    @Content(schema = @Schema(implementation = TrainRecord.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "404", content = {@Content(schema = @Schema())})})
    @PutMapping("/update/{trainNumber}")
    public ResponseEntity<TrainRecord> updateTrain(@PathVariable("trainNumber") String trainNumber,
                                                   @RequestBody TrainRecord trainRecord) {
        TrainRecord _train = trainService.updateTrain(trainRecord, trainNumber);
        if (_train != null) {
            return new ResponseEntity<>(trainService.createTrainRecord(_train), HttpStatus.OK);
        } else {
            return new ResponseEntity<>(HttpStatus.NOT_FOUND);
        }
    }

    @Operation(summary = "Delete a TrainRecord by Id")//, tags = {"tutorials", "delete"})
    @ApiResponses({@ApiResponse(responseCode = "204", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @DeleteMapping("/deleteById/{trainNumber}")
    public ResponseEntity<HttpStatus> deleteTrainById(@PathVariable("trainNumber") String trainNumber) {
        try {
            trainService.deleteByTrainNumber(trainNumber);
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @Operation(summary = "Delete all Trains")//, tags = {"trains", "delete"})
    @ApiResponses({@ApiResponse(responseCode = "204", content = {@Content(schema = @Schema())}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @DeleteMapping("/deleteAll")
    public ResponseEntity<HttpStatus> deleteAllTrains() {
        try {
            trainService.deleteAll();
            return new ResponseEntity<>(HttpStatus.NO_CONTENT);
        } catch (Exception e) {
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }

    }
}

package com.rsv.web.mapper;

import com.rsv.web.models.TrainDetails;
import com.rsv.web.records.TrainRecord;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

public class Mappers {
    public static TrainDetails convertTrainRecordToTrainModel(TrainRecord trainRecord) {
        TrainDetails.TrainDetailsBuilder trainDetailsBuilder = TrainDetails.builder();
        trainDetailsBuilder.id(trainRecord.id());
        trainDetailsBuilder.trainName(trainRecord.trainName());
        trainDetailsBuilder.trainNumber(trainRecord.trainNumber());
        trainDetailsBuilder.totalCoach(trainRecord.totalCoach());
        trainDetailsBuilder.eachCoach(trainRecord.eachCoach());
        trainDetailsBuilder.sourceLocation(trainRecord.sourceLocation());
        trainDetailsBuilder.destinationLocation(trainRecord.destinationLocation());
        trainDetailsBuilder.price(trainRecord.price());

        return trainDetailsBuilder.build();
    }

    public static TrainRecord convertTrainModelToTrainRecord(TrainDetails trainDetails) {
        TrainRecord trainRecord =
                new TrainRecord(trainDetails.getId(),
                        trainDetails.getTrainNumber(),
                        trainDetails.getTrainName(),
                        trainDetails.getTotalCoach(),
                        trainDetails.getEachCoach(),
                        trainDetails.getPrice(),
                        trainDetails.getSourceLocation(),
                        trainDetails.getDestinationLocation()
                );
        return trainRecord;
    }

    public static List<TrainRecord> convertTrainModelToTrainRecordList(List<TrainDetails> trainDetailsList) {
        return Optional.ofNullable(trainDetailsList)
                .orElseGet(Collections::emptyList)
                .stream()
                .map(m -> {
                    TrainRecord trainRecord = convertTrainModelToTrainRecord(m);
                    return trainRecord;
                }).toList();
    }
}

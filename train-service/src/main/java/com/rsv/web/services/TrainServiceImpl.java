package com.rsv.web.services;

import com.rsv.web.mapper.Mappers;
import com.rsv.web.models.TrainDetails;
import com.rsv.web.records.TrainRecord;
import com.rsv.web.repos.TrainRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class TrainServiceImpl implements TrainService {

    private TrainRepository trainRepository;

    @Autowired
    public TrainServiceImpl setTrainRepository(TrainRepository trainRepository) {
        this.trainRepository = trainRepository;
        return this;
    }

    @Override
    public TrainRecord createTrainRecord(TrainRecord trainRecord) {

        TrainDetails trainDetails = Mappers.convertTrainRecordToTrainModel(trainRecord);
        trainRepository.save(trainDetails);
        trainRecord = Mappers.convertTrainModelToTrainRecord(trainDetails);
        return trainRecord;
    }

    @Override
    public List<TrainRecord> findAll() {
        List<TrainDetails> trainDetailsList = trainRepository.findAll();
        List<TrainRecord> trainRecords = Mappers.convertTrainModelToTrainRecordList(trainDetailsList);
        return trainRecords;
    }

    @Override
    public TrainRecord findByTrainNumber(String trainNumber) {
        return trainRepository.findByTrainNumber(trainNumber);
    }

    @Override
    public void deleteByTrainNumber(String trainNumber) {
        trainRepository.deleteByTrainNumber(trainNumber);
    }

    @Override
    public void deleteAll() {

    }

    @Override
    public TrainRecord updateTrain(TrainRecord trainRecord, String trainNumber) {
        TrainDetails trainDetails = Mappers.convertTrainRecordToTrainModel(trainRecord);
        trainRepository.save(trainDetails);
        trainRecord = Mappers.convertTrainModelToTrainRecord(trainDetails);
        return trainRecord;
    }
}

package com.rsv.web.services;

import com.rsv.web.records.TrainRecord;

import java.util.List;

public interface TrainService {
    TrainRecord createTrainRecord(TrainRecord trainRecord);

    List<TrainRecord> findAll();

    TrainRecord findByTrainNumber(String trainNumber);

    void deleteByTrainNumber(String trainNumber);

    void deleteAll();

    TrainRecord updateTrain(TrainRecord trainRecord,String trainNumber);
}

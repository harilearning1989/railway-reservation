package com.rsv.web.repos;

import com.rsv.web.models.TrainDetails;
import com.rsv.web.records.TrainRecord;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TrainRepository extends JpaRepository<TrainDetails, Integer> {
    TrainRecord findByTrainNumber(String trainNumber);

    void deleteByTrainNumber(String trainNumber);

    boolean existsByTrainName(String trainName);

    boolean existsByTrainNumber(String trainNumber);
}

package com.web.util.filters;

import com.web.util.constants.ApplicationConstants;
import com.web.util.utils.JwtTokenUtils;
import com.web.util.validators.RouterValidator;
import jakarta.servlet.FilterChain;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import java.io.IOException;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;

import java.util.Collections;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Component
@Order(1)
public class AuthenticationFilter extends OncePerRequestFilter {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticationFilter.class);
    //private static final String[] excludedEndpoints = new String[]{"/auth/signing, */bar/**"};
    @Value("${rsv.filter.url.ignore}")
    private List<String> excludedEndpoints;


    @Autowired
    private Environment environment;
    private JwtTokenUtils jwtTokenUtils;
    private RouterValidator routerValidator;

    @Autowired
    public AuthenticationFilter setJwtTokenUtils(JwtTokenUtils jwtTokenUtils) {
        this.jwtTokenUtils = jwtTokenUtils;
        return this;
    }

    @Autowired
    public AuthenticationFilter setRouterValidator(RouterValidator routerValidator) {
        this.routerValidator = routerValidator;
        return this;
    }

    @Override
    protected void doFilterInternal(
            HttpServletRequest request,
            HttpServletResponse response,
            FilterChain filterChain) throws ServletException, IOException {
        String path = request.getServletPath();
        System.out.println("Path:::::::" + path);
        if (routerValidator.isSecured.test(path)) {
            LOGGER.info("Path::" + path
                    + "===Environment jwt enabled::" + environment.getProperty("jwt.enabled"));
            String jwtEnabled = environment.getProperty("jwt.enabled");
            if (StringUtils.isNotBlank(jwtEnabled)
                    && StringUtils.equalsAnyIgnoreCase(jwtEnabled, "true")) {
                Map<String, List<String>> headersMap = Collections.list(request.getHeaderNames())
                        .stream()
                        .collect(Collectors.toMap(
                                Function.identity(),
                                h -> Collections.list(request.getHeaders(h))
                        ));
                headersMap.forEach((k, v) -> {
                    v.forEach(f -> {
                        System.out.println("Key::" + k + "===Value::" + f);
                    });
                });
                final String authHeader = request.getHeader(HttpHeaders.AUTHORIZATION);
                String authHeaderTemp = request.getHeader("authorization");
                LOGGER.info("JwtTokenFilter enters into doFilterInternal authHeaderTemp::" + authHeaderTemp);
                if (StringUtils.isEmpty(authHeader) || (!StringUtils.startsWith(authHeader, ApplicationConstants.BEARER_PREFIX)
                        && !StringUtils.startsWith(authHeader, ApplicationConstants.BASIC_PREFIX))) {
                    LOGGER.info("JwtTokenFilter enters into authHeader condition");
                    jwtTokenUtils.generateErrorMesage(response, ApplicationConstants.TOKEN_NOT_FOUND, HttpStatus.FORBIDDEN.value());
                    return;
                }
                if (StringUtils.startsWith(authHeader, ApplicationConstants.BEARER_PREFIX)) {
                    final String jwtToken = authHeader.split(" ")[1].trim();
                    LOGGER.info("JwtTokenFilter enters into BEARER_PREFIX condition jwtToken::" + jwtToken);
                    try {
                        jwtTokenUtils.validateJwtToken(jwtToken);
                    } catch (Exception e) {
                        LOGGER.info("Exception message::" + e.getMessage());
                        jwtTokenUtils.generateErrorMesage(response, ApplicationConstants.TOKEN_VALIDATION_FAILED, HttpStatus.UNAUTHORIZED.value());
                        return;
                    }
                } else {
                    LOGGER.info(ApplicationConstants.BEARER_PREFIX_NOT_FOUND);
                    jwtTokenUtils.generateErrorMesage(response, ApplicationConstants.BEARER_PREFIX_NOT_FOUND, HttpStatus.FORBIDDEN.value());
                    return;
                }
            }
        }
        filterChain.doFilter(request, response);
    }

    @Override
    protected boolean shouldNotFilter(HttpServletRequest request) throws ServletException {
        String jwtEnabled = environment.getProperty("jwt.enabled");
        if (StringUtils.isNotBlank(jwtEnabled)
                && StringUtils.equalsAnyIgnoreCase(jwtEnabled, "false")) {
            LOGGER.info("JWT authentication filter disabled::"+jwtEnabled);
            return true;
        }
        LOGGER.info("JwtTokenFilter enters into shouldNotFilter requestUri::"
                + request.getServletPath() + "====excludedEndpoints::" + excludedEndpoints);
        boolean filterStatus = excludedEndpoints.stream()
                .anyMatch(s -> s.contains(request.getServletPath())
                        || request.getServletPath().contains("swagger")
                        || request.getServletPath().contains("api-docs"));
        LOGGER.info("filterStatus::" + filterStatus);
        return filterStatus;
    }

}

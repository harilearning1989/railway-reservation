package com.web.util.constants;

public class ApplicationConstants {
    public static final String BEARER_PREFIX = "Bearer ";
    public static final String BEARER_PREFIX_NOT_FOUND = "Bearer Not found";
    public static final String BASIC_PREFIX = "Basic ";
    public static final String TOKEN_NOT_FOUND = "There is no token in the header ";
    public static final String TOKEN_VALIDATION_FAILED = "Token validation failed";

    public static final String DOWNLOAD_LOCATION = "D:/DataFiles/Downloaded/";
    public static final String DOWNLOAD_MAC = "/Users/hariduddukunta/MyWork/DataFiles/";

}


package com.web.util.constants;

public interface CommonConstants {
    String ADMIN = "Admin";
    String USER = "User";
    String REGISTER_SUCCESS = "Successfully Registered %s with the Name: %s ";
    String UPDATED_SUCCESS = "Successfully Updated %s with the Name: %s ";
    String NOT_FOUND_WITH_ID = "%s with id %d not found";
    String DELETED_SUCCESS = "Successfully deleted %s with the Name: %s ";
    String USER_NAME_ALREADY_EXISTS = "User Already Exists with the Name: %s ";
    String USER_EMAIL_ALREADY_EXISTS = "Email Already Exists with the Name: %s ";
    String USER_ROLE_NOT_FOUND = "User Role Not Found, please provide valid Role: %s ";
    String SINGLE_SPACE = " ";
    String ADMIN_NAME_ALREADY_EXISTS = "Admin Already Exists with the Name: %s ";
    String ADMIN_ID_ALREADY_EXISTS = "AdminId Already Exists with the Id: %s ";
    String USER_ID_ALREADY_EXISTS = "UserId Already Exists with the Id: %s ";

}

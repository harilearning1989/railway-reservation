package com.web.util.utils;

import com.web.util.response.RsvApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

public class ResponseUtil {

    public static <T> ResponseEntity<RsvApiResponse<T>> createResponse(HttpStatus status, String message, T data) {
        RsvApiResponse<T> apiResponse = new RsvApiResponse<>(status.value(), message, data);
        return new ResponseEntity<>(apiResponse, status);
    }

    public static <T> ResponseEntity<RsvApiResponse<T>> createNewTrainResponse(T data) {
        return createResponse(HttpStatus.CREATED, "Created Successfully", data);
    }
    public static <T> ResponseEntity<RsvApiResponse<T>> createSuccessResponse(T data) {
        return createResponse(HttpStatus.OK, "Success", data);
    }

    public static <T> ResponseEntity<RsvApiResponse<T>> createErrorResponse(String message) {
        return createResponse(HttpStatus.BAD_REQUEST, message, null);
    }

    public static <T> ResponseEntity<RsvApiResponse<T>> createNoDataResponse(String message) {
        return createResponse(HttpStatus.NO_CONTENT, message, null);
    }

    public static <T> ResponseEntity<RsvApiResponse<T>> createServerErrorResponse(String message) {
        return createResponse(HttpStatus.INTERNAL_SERVER_ERROR, message, null);
    }
}


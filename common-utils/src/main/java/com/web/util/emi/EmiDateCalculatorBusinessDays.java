package com.web.util.emi;

import java.time.LocalDate;
import java.time.DayOfWeek;
import java.util.HashSet;
import java.util.Set;

public class EmiDateCalculatorBusinessDays {

    public static void main(String[] args) {
        LocalDate startDate = LocalDate.of(2024, 7, 21); // Starting date

        Set<LocalDate> holidays = new HashSet<>();
        holidays.add(LocalDate.of(2024, 12, 25)); // Example holiday: Christmas
        holidays.add(LocalDate.of(2024, 1, 1));  // Example holiday: New Year's Day

        System.out.println("Daily: " + calculateNextBusinessDay(startDate, "daily", holidays));
        System.out.println("Weekly: " + calculateNextBusinessDay(startDate, "weekly", holidays));
        System.out.println("Monthly: " + calculateNextBusinessDay(startDate, "monthly", holidays));
        System.out.println("Quarterly: " + calculateNextBusinessDay(startDate, "quarterly", holidays));
        System.out.println("Half-Yearly: " + calculateNextBusinessDay(startDate, "half-yearly", holidays));
        System.out.println("Yearly: " + calculateNextBusinessDay(startDate, "yearly", holidays));
    }

    public static LocalDate calculateNextBusinessDay(LocalDate startDate, String frequency, Set<LocalDate> holidays) {
        LocalDate nextDate = startDate;

        // Ensure the starting date is a business day
        nextDate = adjustToNextBusinessDay(nextDate, holidays);

        switch (frequency.toLowerCase()) {
            case "daily":
                nextDate = nextDate.plusDays(1);
                break;
            case "weekly":
                nextDate = nextDate.plusWeeks(1);
                break;
            case "monthly":
                nextDate = nextDate.plusMonths(1);
                break;
            case "quarterly":
                nextDate = nextDate.plusMonths(3);
                break;
            case "half-yearly":
                nextDate = nextDate.plusMonths(6);
                break;
            case "yearly":
                nextDate = nextDate.plusYears(1);
                break;
            default:
                throw new IllegalArgumentException("Invalid frequency: " + frequency);
        }

        // Ensure the calculated date is a business day
        return adjustToNextBusinessDay(nextDate, holidays);
    }

    public static LocalDate adjustToNextBusinessDay(LocalDate date, Set<LocalDate> holidays) {
        while (date.getDayOfWeek() == DayOfWeek.SATURDAY ||
                date.getDayOfWeek() == DayOfWeek.SUNDAY ||
                holidays.contains(date)) {
            date = date.plusDays(1);
        }
        return date;
    }
}


package com.web.util.exception;

import com.web.util.response.RsvApiResponse;
import com.web.util.utils.ResponseUtil;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class GlobalExceptionHandler {

    @ExceptionHandler(Exception.class)
    public ResponseEntity<RsvApiResponse<String>> handleException(Exception ex) {
        return ResponseUtil.createErrorResponse(ex.getMessage());
    }
}

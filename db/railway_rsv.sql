CREATE TABLE rsv_admins (
    id int NOT NULL AUTO_INCREMENT,
    admin_id varchar(10) NOT NULL,
    first_name varchar(15) NOT NULL,
    last_name varchar(15) NOT NULL,
    username varchar(50) NOT NULL,
    password varchar(100) NOT NULL,
    email varchar(50) NOT NULL,
    PHONE varchar(30) NOT NULL,
    AGE INT,
    GENDER VARCHAR(6),
    STATE_NAME varchar(100),
    created_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT rsv_ADMINS_id_PK PRIMARY KEY (ID),
    CONSTRAINT rsv_ADMINS_ADMIN_ID_unique UNIQUE (ADMIN_ID),
    CONSTRAINT rsv_ADMINS_username_unique UNIQUE (username),
    CONSTRAINT rsv_ADMINS_email_unique UNIQUE (email)
);


CREATE TABLE rsv_users (
    id int NOT NULL AUTO_INCREMENT,
    user_id varchar(10) NOT NULL,
    first_name varchar(15) NOT NULL,
    last_name varchar(15) NOT NULL,
    username varchar(50) NOT NULL,
    password varchar(100) NOT NULL,
    email varchar(50) NOT NULL,
    PHONE varchar(30) NOT NULL,
    created_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT rsv_user_id_PK PRIMARY KEY (id),
    CONSTRAINT rsv_users_user_id_unique UNIQUE (user_id),
    CONSTRAINT rsv_users_username_unique UNIQUE (username),
    CONSTRAINT rsv_users_email_unique UNIQUE (email)
);

CREATE TABLE rsv_roles (
    role_id int NOT NULL AUTO_INCREMENT,
    name varchar(60) NOT NULL,
    created_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP NULL DEFAULT CURRENT_TIMESTAMP,
    CONSTRAINT rsv_roles_role_id_PK PRIMARY KEY (role_id),
    CONSTRAINT rsv_roles_role_name_unique UNIQUE (name)
);


CREATE TABLE `rsv_user_roles` (
  `user_id` int NOT NULL,
  `role_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT rsv_user_roles_id_pk PRIMARY KEY (user_id,role_id),
  CONSTRAINT `fk_rsv_user_roles_role_id` FOREIGN KEY (`role_id`) REFERENCES `rsv_roles` (`role_id`),
  CONSTRAINT `fk_rsv_user_roles_user_id` FOREIGN KEY (`user_id`) REFERENCES `rsv_users` (`id`)
);

CREATE TABLE `rsv_admin_roles` (
  `admin_id` int NOT NULL,
  `role_id` int NOT NULL,
  `created_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT rsv_admin_roles_id_pk PRIMARY KEY (admin_id,role_id),
  CONSTRAINT `fk_rsv_admin_roles_role_id` FOREIGN KEY (`role_id`) REFERENCES `rsv_roles` (`role_id`),
  CONSTRAINT `fk_rsv_admin_roles_admin_id` FOREIGN KEY (`admin_id`) REFERENCES `rsv_admins` (`id`)
);

INSERT INTO rsv_roles(name) VALUES('ROLE_USER');
INSERT INTO rsv_roles(name) VALUES('ROLE_ADMIN');
INSERT INTO rsv_roles(name) VALUES('ROLE_EMPLOYEE');

=============================================================================================


CREATE TABLE RSV_TRAIN_DETAILS (
  ID int NOT NULL AUTO_INCREMENT,
  TRAIN_NAME varchar(100) NOT NULL,
  TRAIN_NUMBER varchar(50) NOT NULL,
  TOTAL_COACH int DEFAULT '0',
  EACH_COACH INT DEFAULT '0',
  PRICE INT DEFAULT '0',
  SOURCE_LOCATION varchar(50) NOT NULL,
  DESTINATION_LOCATION varchar(50) NOT NULL,
  DATE_CREATED timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  LAST_UPDATED timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT RSV_TRAIN_DETAILS_ID_PK PRIMARY KEY (ID)
);

ALTER TABLE RSV_TRAIN_DETAILS ADD CONSTRAINT RSV_TRAIN_DETAILS_UNIQUE UNIQUE (TRAIN_NAME, TRAIN_NUMBER);

CREATE TABLE `EMPLOYEE` (
  `EMP_ID` int NOT NULL AUTO_INCREMENT,
  `EMP_NAME` varchar(50) DEFAULT NULL,
  `USERNAME` varchar(50) DEFAULT NULL,
  `EMAIL` varchar(60) DEFAULT NULL,
  `AGE` int DEFAULT '0',
  `PHONE` varchar(30) DEFAULT NULL,
  `GENDER` varchar(6) DEFAULT NULL,
  `salary` bigint DEFAULT '0',
  `DATE_CREATED` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `LAST_UPDATED` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  CONSTRAINT rsv_employee_emp_id_PK PRIMARY KEY (EMP_ID)
);

DELETE n1 FROM EMPLOYEE n1, EMPLOYEE n2 WHERE n1.emp_id > n2.emp_id AND n1.username = n2.username;
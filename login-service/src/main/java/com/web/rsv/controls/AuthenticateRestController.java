package com.web.rsv.controls;

import com.web.rsv.mappers.DataMappers;
import com.web.rsv.models.Admin;
import com.web.rsv.models.Role;
import com.web.rsv.models.User;
import com.web.rsv.models.UserPrincipal;
import com.web.rsv.records.AdminRecord;
import com.web.rsv.records.LoginRequest;
import com.web.rsv.records.SignupRequest;
import com.web.rsv.records.response.JwtResponse;
import com.web.rsv.repos.AdminRepository;
import com.web.rsv.repos.UserRepository;
import com.web.util.constants.CommonConstants;
import com.web.util.response.GlobalResponse;
import com.web.util.response.ResponseHandler;
import com.web.util.utils.JwtTokenUtils;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import io.swagger.v3.oas.annotations.tags.Tag;
import jakarta.validation.Valid;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;

import java.util.*;
import java.util.stream.Collectors;

@Tag(name = "Admin Details", description = "Admin management APIs")
@CrossOrigin(origins = "*")
@RestController
@RequestMapping("auth")
public class AuthenticateRestController {

    private static final Logger LOGGER = LoggerFactory.getLogger(AuthenticateRestController.class);

    AuthenticationManager authenticationManager;
    UserRepository userRepository;
    AdminRepository adminRepository;
    JwtTokenUtils jwtTokenUtils;
    @Autowired
    DataMappers dataMappers;

    @Autowired
    public AuthenticateRestController setAuthenticationManager(AuthenticationManager authenticationManager) {
        this.authenticationManager = authenticationManager;
        return this;
    }

    @Autowired
    public AuthenticateRestController setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
        return this;
    }

    @Autowired
    public AuthenticateRestController setJwtTokenUtils(JwtTokenUtils jwtTokenUtils) {
        this.jwtTokenUtils = jwtTokenUtils;
        return this;
    }

    @Autowired
    public AuthenticateRestController setAdminRepository(AdminRepository adminRepository) {
        this.adminRepository = adminRepository;
        return this;
    }

    @Operation(summary = "Create a new Admin Record", description = "Create Admin Record")
    @ApiResponses({
            @ApiResponse(responseCode = "201", content = {
                    @Content(schema = @Schema(implementation = AdminRecord.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @PostMapping("/signIn")
    public ResponseEntity<?> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        LOGGER.info("authenticateUser: loginRequest={}", loginRequest);
        Authentication authentication =
                authenticationManager.authenticate(
                        new UsernamePasswordAuthenticationToken(loginRequest.username(),
                                loginRequest.password()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        /*UserDetailsImpl userDetails = (UserDetailsImpl) authentication.getPrincipal();
        Map<String, Object> claims = new HashMap<>();
        claims.put("username", userDetails.getUsername());
        claims.put("email", userDetails.getEmail());
        claims.put("roles", userDetails.getAuthorities());
        String jwt = jwtTokenUtils.generateJwtToken(claims);

        List<String> roles = userDetails.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(
                new JwtResponse(jwt,
                        userDetails.getId(),
                        userDetails.getUsername(),
                        userDetails.getEmail(),
                        roles));*/

        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        Map<String, Object> claims = new HashMap<>();
        claims.put("username", userPrincipal.getUsername());
        claims.put("email", userPrincipal.getEmail());
        claims.put("roles", userPrincipal.getAuthorities());
        String jwt = jwtTokenUtils.generateJwtToken(claims);

        List<String> roles = userPrincipal.getAuthorities().stream()
                .map(item -> item.getAuthority())
                .collect(Collectors.toList());

        return ResponseEntity.ok(
                new JwtResponse(jwt,
                        userPrincipal.id(),
                        userPrincipal.getUsername(),
                        userPrincipal.getEmail(),
                        roles));
    }

    @Operation(summary = "Create a new Admin Record", description = "Create Admin Record")
    @ApiResponses({
            @ApiResponse(responseCode = "201", content = {
                    @Content(schema = @Schema(implementation = AdminRecord.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @PostMapping("/user/create")
    public GlobalResponse registerUser(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userRepository.existsByUserId(signUpRequest.userId())) {
            LOGGER.info("User Already Exists with the Id::{}", signUpRequest.userId());
            return ResponseHandler.generateResponse(
                    String.format(CommonConstants.ADMIN_ID_ALREADY_EXISTS,
                            signUpRequest.userId()), HttpStatus.BAD_REQUEST, null);
        }
        if (userRepository.existsByUsername(signUpRequest.username())) {
            return ResponseHandler.generateResponse(
                    String.format(CommonConstants.USER_NAME_ALREADY_EXISTS,
                            signUpRequest.username()), HttpStatus.BAD_REQUEST, null);
        }
        if (userRepository.existsByEmail(signUpRequest.email())) {
            return ResponseHandler.generateResponse(
                    String.format(CommonConstants.USER_EMAIL_ALREADY_EXISTS,
                            signUpRequest.username()), HttpStatus.BAD_REQUEST, null);
        }
        if (signUpRequest.roles() == null || signUpRequest.roles().isEmpty()) {
            return ResponseHandler.generateResponse(
                    String.format(CommonConstants.USER_ROLE_NOT_FOUND,
                            signUpRequest.username()), HttpStatus.BAD_REQUEST, null);
        }
        User user = dataMappers.recordToEntity(signUpRequest);

        Set<String> strRoles = new HashSet<>(Arrays.asList(signUpRequest.roles()));
        Set<Role> roles = dataMappers.getRoles(strRoles);

        user.setRoles(roles);
        userRepository.save(user);

        user.setPassword(null);

        return ResponseHandler.generateResponse(
                String.format(CommonConstants.REGISTER_SUCCESS,
                        CommonConstants.USER, signUpRequest.username()), HttpStatus.OK, user);
    }

    @Operation(summary = "Create a new Admin Record", description = "Create Admin Record")
    @ApiResponses({
            @ApiResponse(responseCode = "201", content = {
                    @Content(schema = @Schema(implementation = AdminRecord.class), mediaType = "application/json")}),
            @ApiResponse(responseCode = "500", content = {@Content(schema = @Schema())})})
    @PostMapping("/admin/create")
    public GlobalResponse registerAdmin(@Valid @RequestBody AdminRecord adminRecord) {
        LOGGER.info("Entered into register Admin");
        if (adminRepository.existsByAdminId(adminRecord.adminId())) {
            LOGGER.info("Admin Already Exists with the Id::{}", adminRecord.adminId());
            return ResponseHandler.generateResponse(
                    String.format(CommonConstants.ADMIN_ID_ALREADY_EXISTS,
                            adminRecord.adminId()), HttpStatus.BAD_REQUEST, null);
        }
        if (adminRepository.existsByUsername(adminRecord.username())) {
            LOGGER.info("Admin Already Exists with the username::{}", adminRecord.username());
            return ResponseHandler.generateResponse(
                    String.format(CommonConstants.ADMIN_NAME_ALREADY_EXISTS,
                            adminRecord.username()), HttpStatus.BAD_REQUEST, null);
        }
        if (adminRepository.existsByEmail(adminRecord.email())) {
            LOGGER.info("Admin Already Exists with the email::{}", adminRecord.email());
            return ResponseHandler.generateResponse(
                    String.format(CommonConstants.USER_EMAIL_ALREADY_EXISTS,
                            adminRecord.email()), HttpStatus.BAD_REQUEST, null);
        }
        if (adminRecord.roles() == null || adminRecord.roles().isEmpty()) {
            LOGGER.info("Role is empty with the username::{}", adminRecord.username());
            return ResponseHandler.generateResponse(
                    String.format(CommonConstants.USER_ROLE_NOT_FOUND,
                            adminRecord.username()), HttpStatus.BAD_REQUEST, null);
        }
        Admin admin = dataMappers.recordToEntity(adminRecord);

        Set<String> strRoles = new HashSet<>(Arrays.asList(adminRecord.roles()));
        Set<Role> roles = dataMappers.getRoles(strRoles);

        admin.setRoles(roles);
        adminRepository.save(admin);

        return ResponseHandler.generateResponse(
                String.format(CommonConstants.REGISTER_SUCCESS,
                        CommonConstants.ADMIN, adminRecord.username()), HttpStatus.OK, admin);
    }
/*

    @PostMapping("/signupTmp")
    public ResponseEntity<?> registerUserTemp(@Valid @RequestBody SignupRequest signUpRequest) {
        if (userRepository.existsByUsername(signUpRequest.username())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Username is already taken!"));
        }

        if (userRepository.existsByEmail(signUpRequest.email())) {
            return ResponseEntity
                    .badRequest()
                    .body(new MessageResponse("Error: Email is already in use!"));
        }

        User user = User.builder()
                .username(signUpRequest.username())
                .email(signUpRequest.email())
                .password(encoder.encode(signUpRequest.password()))
                .phone(signUpRequest.phone())
                .build();

        Set<String> strRoles = signUpRequest.role();
        Set<Role> roles = new HashSet<>();

        if (strRoles == null) {
            Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(adminRole);

                        break;
                    case "patient":
                        Role patientRole = roleRepository.findByName(RoleName.ROLE_PATIENT)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(patientRole);

                        break;
                    case "doctor":
                        Role doctorRole = roleRepository.findByName(RoleName.ROLE_DOCTOR)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(doctorRole);

                        break;
                    default:
                        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
                        roles.add(userRole);
                }
            });
        }

        user.setRoles(roles);
        userRepository.save(user);

        return ResponseEntity.ok(new MessageResponse("User registered successfully!"));
    }
*/

}


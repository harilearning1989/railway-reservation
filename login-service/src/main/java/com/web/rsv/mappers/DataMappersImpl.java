package com.web.rsv.mappers;

import com.web.rsv.models.Admin;
import com.web.rsv.models.Role;
import com.web.rsv.models.RoleName;
import com.web.rsv.models.User;
import com.web.rsv.records.AdminRecord;
import com.web.rsv.records.SignupRequest;
import com.web.rsv.repos.RoleRepository;
import com.web.util.response.UserResponse;
import com.web.util.utils.CommonUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Component
public class DataMappersImpl implements DataMappers {

    private static final Logger LOGGER = LoggerFactory.getLogger(DataMappersImpl.class);

    @Autowired
    PasswordEncoder encoder;
    @Autowired
    RoleRepository roleRepository;

    @Override
    public UserResponse entityToRecord(User user) {
        //Wed Aug 16 12:29:39 IST 2023
        String createdDateTmp = CommonUtils.convertDateToString(user.getCreatedDate());
        String updatedDateTmp = CommonUtils.convertDateToString(user.getUpdatedDate());

        UserResponse userResponse = new UserResponse(
                user.getUserId(),
                user.getUsername(),
                user.getEmail(),
                user.getPhoneNumber(),
                null,
                createdDateTmp,
                updatedDateTmp
        );
        return userResponse;
    }

    @Override
    public Admin recordToEntity(AdminRecord adminRecord) {
        Admin.AdminBuilder adminBuilder = Admin.builder();
        adminBuilder.id(adminRecord.id());
        adminBuilder.adminId(adminRecord.adminId());
        adminBuilder.firstName(adminRecord.firstName());
        adminBuilder.lastname(adminRecord.lastName());
        adminBuilder.username(adminRecord.username());
        adminBuilder.password(encoder.encode(adminRecord.password()));
        adminBuilder.email(adminRecord.email());
        adminBuilder.phone(String.valueOf(adminRecord.phoneNumber()));
        adminBuilder.gender(adminRecord.gender());
        adminBuilder.age(adminRecord.age());
        adminBuilder.stateName(adminRecord.stateName());
        adminBuilder.createdDate(new Date());
        adminBuilder.updatedDate(new Date());

        return adminBuilder.build();
    }

    @Override
    public User recordToEntity(SignupRequest signUpRequest) {
        User.UserBuilder userBuilder = User.builder();
        userBuilder.userId(signUpRequest.userId());
        userBuilder.firstName(signUpRequest.firstName());
        userBuilder.lastname(signUpRequest.lastName());
        userBuilder.username(signUpRequest.username());
        userBuilder.password(encoder.encode(signUpRequest.password()));
        userBuilder.email(signUpRequest.email());
        userBuilder.phoneNumber(signUpRequest.phoneNumber());
        userBuilder.createdDate(new Date());
        userBuilder.updatedDate(new Date());

        return userBuilder.build();
    }

    @Override
    public Set<Role> getRoles(Set<String> strRoles) {
        Set<Role> roles = new HashSet<>();
        if (strRoles.isEmpty()) {
            Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                    .orElseThrow(() -> new RuntimeException("Error: Role is not found."));
            roles.add(userRole);
        } else {
            strRoles.forEach(role -> {
                switch (role) {
                    case "admin":
                        Role adminRole = roleRepository.findByName(RoleName.ROLE_ADMIN)
                                .orElseThrow(() -> new RuntimeException("Error:Admin Role is not found."));
                        roles.add(adminRole);

                        break;
                    case "employee":
                        Role employeeRole = roleRepository.findByName(RoleName.ROLE_EMPLOYEE)
                                .orElseThrow(() -> new RuntimeException("Error: Employee Role is not found."));
                        roles.add(employeeRole);

                        break;
                    default:
                        Role userRole = roleRepository.findByName(RoleName.ROLE_USER)
                                .orElseThrow(() -> new RuntimeException("Error:User Role is not found."));
                        roles.add(userRole);
                }
            });
        }
        return roles;
    }

}

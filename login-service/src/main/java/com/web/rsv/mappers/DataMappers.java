package com.web.rsv.mappers;

import com.web.rsv.models.Admin;
import com.web.rsv.models.Role;
import com.web.rsv.models.User;
import com.web.rsv.records.AdminRecord;
import com.web.rsv.records.SignupRequest;
import com.web.util.response.UserResponse;

import java.util.Set;

public interface DataMappers {

    UserResponse entityToRecord(User user);

    Admin recordToEntity(AdminRecord adminRecord);

    User recordToEntity(SignupRequest signupRequest);

    Set<Role> getRoles(Set<String> strRoles);
}

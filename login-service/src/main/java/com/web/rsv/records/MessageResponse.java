package com.web.rsv.records;

public record MessageResponse(
        String message
) {
}

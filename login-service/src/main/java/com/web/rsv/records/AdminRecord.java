package com.web.rsv.records;

import java.util.Set;

public record AdminRecord(
        int id,
        String adminId,
        String firstName,
        String lastName,
        String username,
        String password,
        String email,
        long phoneNumber,
        int age,
        String gender,
        String roles,
        String stateName,
        String createdAt,
        String updatedAt
) {
}

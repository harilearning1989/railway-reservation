package com.web.rsv.records;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;

import java.util.Set;

//@JsonIgnoreProperties({"password"})
//@JsonIgnoreProperties(value = {"property1"}, allowGetters = true, allowSetters = false)
//@JsonInclude(JsonInclude.Include.NON_NULL)
public record SignupRequest(
        long id,
        long userId,
        @NotBlank
        @Size(min = 3, max = 20)
        String firstName,
        @NotBlank
        @Size(min = 3, max = 20)
        String lastName,
        @NotBlank
        @Size(min = 3, max = 20)
        String username,
        @NotBlank
        @Size(min = 6, max = 40)
        String password,
        @NotBlank
        @Size(max = 50)
        @Email
        String email,
        long phoneNumber,
        String gender,
        String roles) {
}


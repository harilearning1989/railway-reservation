package com.web.rsv.models;

public enum RoleName {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_EMPLOYEE,
}

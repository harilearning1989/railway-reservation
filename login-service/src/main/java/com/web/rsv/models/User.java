package com.web.rsv.models;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.NaturalId;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

@Entity
@Table(name = "RSV_USERS", uniqueConstraints = {
        @UniqueConstraint(columnNames = {
                "USER_NAME"
        }),
        @UniqueConstraint(columnNames = {
                "email"
        })
})
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties({"password"})
public class User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private Long id;

    @Column(name = "user_id")
    private Long userId;

    @NotBlank
    @Size(max = 25)
    @Column(name = "FIRST_NAME")
    private String firstName;

    @NotBlank
    @Size(max = 25)
    @Column(name = "LAST_NAME")
    private String lastname;

    @NotBlank
    @Size(max = 25)
    @Column(name = "USERNAME")
    private String username;

    @NotBlank
    @Size(max = 100)
    @Column(name = "PASSWORD")
    private String password;

    @NaturalId
    @NotBlank
    @Size(max = 40)
    @Email
    @Column(name = "EMAIL")
    private String email;

    @Column(name = "PHONE")
    private long phoneNumber;

    @Column(name = "CREATED_AT")
    private Date createdDate;
    @Column(name = "UPDATED_AT")
    private Date updatedDate;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(
            name = "RSV_USER_ROLES",
            joinColumns = @JoinColumn(name = "user_id"),
            inverseJoinColumns = @JoinColumn(name = "role_id"))
    private Set<Role> roles;
}

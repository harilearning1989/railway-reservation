package com.web.rsv.services;

import com.web.rsv.models.Admin;
import com.web.rsv.models.User;
import com.web.rsv.models.UserPrincipal;
import com.web.rsv.repos.AdminRepository;
import com.web.rsv.repos.UserRepository;
import com.web.util.exception.UserNotExistsException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Optional;

@Service("userDetailsService")
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private AdminRepository adminRepository;

    @Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Optional<Admin> adminOpt = adminRepository.findByUsername(username);
        if (adminOpt.isPresent()) {
            Admin admin = adminOpt.get();
            return UserPrincipal.create(admin);
        }

        Optional<User> userOpt = userRepository.findByUsername(username);
        if (userOpt.isPresent()) {
            User user = userOpt.get();
            return UserPrincipal.create(user);
        }

        throw new UsernameNotFoundException("User not found with username: " + username);
    }

    // This method is used by JWTAuthenticationFilter
    public UserDetails loadUserById(Long id) {
        Optional<Admin> adminOpt = adminRepository.findById(id);
        if (adminOpt.isPresent()) {
            return UserPrincipal.create(adminOpt.get());
        }

        Optional<User> userOpt = userRepository.findById(id);
        if (userOpt.isPresent()) {
            return UserPrincipal.create(userOpt.get());
        }

        throw new UsernameNotFoundException("User not found with id: " + id);
    }

    /*@Override
    @Transactional
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userRepository.findByUsername(username)
                .orElseThrow(() -> new UserNotExistsException("User Not Found with Username=" + username));
        return UserDetailsImpl.build(user);
    }*/

}

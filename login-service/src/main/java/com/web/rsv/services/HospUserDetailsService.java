package com.web.rsv.services;

import com.web.util.response.UserResponse;

import java.util.List;

public interface HospUserDetailsService {
    List<UserResponse> listAllUsers();

    String deleteUserById(long userId);
}
